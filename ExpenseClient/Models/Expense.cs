﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace ExpenseClient.Models
{
    public class Expense
    {
        public int EmployeeNum { get; set; }         
        public bool Reimburse { get; set; }
        public DateTime ExpenseDate { get; set; }
        public string CategoryID { get; set; }
        public int Attendees { get; set; }
        public int Mileage { get; set; }
        public string Place { get; set; }
        public string Purpose { get; set; }
        public string PersonsEntertained { get; set; }
        public int Amount { get; set; }
        
    }
}