﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExpenseClient.Models
{
    public class FCMPushNotification
    {
        public FCMPushNotification()
        {
            // TODO: Add constructor logic here
        }

        public bool Successful
        {
            get;
            set;
        }

        public string Response
        {
            get;
            set;
        }
        public Exception Error
        {
            get;
            set;
        }


    }
}