﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace ExpenseClient.ViewModels
{
    public class MaintWorkOrderViewModel
    {
        

        public DateTime Date { get; set; }
        public string Username { get; set; }
        

        [Display(Name= "Machine")]
        public List<MachineDropDown> MachineDropDown { get; set; }

        [Display(Name = "Urgency")]
        public List<MNTUrgencyDropDown> UrgencyDropDown { get; set; }
        
        [Display(Name = "Department")]
        public List<DeptDropDown> DeptDropDown { get; set; }
          
        public string Importance { get; set; } // string value
        public int numericImportance { get; set; }
        public bool Emergency { get; set; }
        public string Description { get; set; }
        public string MNTComments { get; set; }
        public string Status { get; set; }      
        
        public int WorkOrderNumber { get; set; }
        public string Message { get; set; }
        public int machineID { get; set; }
        public string DepartmentNum { get; set; }

    }

    public class MNTUrgencyDropDown
    {
        public string Urgency { get; set; }
       
    }

    public class MachineDropDown
    {
        public string Machine { get; set; }
        public string MachineID { get; set; }
    }

    public class DeptDropDown
    {
        public string Dept { get; set; }
        public string DeptID { get; set; }
    }
}
