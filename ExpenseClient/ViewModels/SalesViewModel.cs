﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExpenseClient.ViewModels
{
    public class SalesViewModel
    {
        [Display(Name = "Start Date")]
        public DateTime PromiseDateBegin { get; set; }

        [Display(Name = "End Date")]
        public DateTime PromiseDateEnd { get; set; }

        [Display(Name = "Customer Number(if required)")]
        public int CustomerNumber { get; set; }

        [Display(Name = "Sales Reports")]
        public List<SalesDropDown> salesDropDownProperty { get; set; }

        public string report { get; set; }               

        public class SalesDropDown
        {
            public string selected { get; set; }
        }
    }
}