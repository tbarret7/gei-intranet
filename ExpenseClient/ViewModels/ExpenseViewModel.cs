﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ExpenseClient.Models;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace ExpenseClient.ViewModels
{
    public class ExpenseViewModel
    {
        
        [Display(Name = "Badge Number")]
        public string EmployeeNum { get; set; }

        
        [Display(Name = "User")]
        public string EmployeeName { get; set; }

        public DateTime WeekEndDate { get; set; }
        
        
        [Display(Name = "Date of Expense")]
        public DateTime? ExpenseDate { get; set; }

        public bool Reimburse { get; set; }
        
        
        [Display(Name = "Number of Attendees")]
        public int? Attendees { get; set; } 

        
        public decimal? Mileage { get; set; } 

        
        [Display(Name = "Place/City/State")]
        public string Place { get; set; }

        
        [Display(Name = "Business Purpose")]
        public string Purpose { get; set; }

       
        [Display(Name = "Persons Entertained")]
        public string PersonsEntertained { get; set; }

        
        public decimal? Amount { get; set; }

        
        [Display(Name = "Category")]
        public List<CatDropDown> CategoryDropDownProperty { get; set; } //property that refers the model that fills the drop down
        public string categoryid { get; set; } //property that holds drop down list values

        public DateTime Date { get; set; }

        public string AccountNum { get; set; }

        public bool CashAdvance { get; set; }

        
        [Display(Name = "Advanced Amount")]
        public float AdvanceAmt { get; set; }

        public bool Approved { get; set; }
        public int Line { get; set; }

        [Display(Name = "Select Date")]
        public DateTime? selDate { get; set; }
        
    }
    public class CatDropDown
    {
        public string CategoryID { get; set; }
        public string Categoryname { get; set; }
        public string Accountnum { get; set; }
    }
       
   
}