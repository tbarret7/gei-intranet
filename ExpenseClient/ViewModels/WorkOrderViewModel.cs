﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace ExpenseClient.ViewModels
{
    public class WorkOrderViewModel
    {
        [Display(Name ="Category")]
        public List<CategoryDropDown> CategoryDropDown { get; set; }
        public string category { get; set; }

        public DateTime? Date { get; set; }
        public string Username { get; set; }
        public string Comp_Name { get; set; }

        [Display(Name = "Urgency")]
        public List<UrgencyDropDown> UrgencyDropDown { get; set; }
        public string Importance { get; set; }

        public bool Emergency { get; set; }

        public string Description { get; set; }
        public string ISComments { get; set; }
        public string Status { get; set; }
        public DateTime? StatusDate { get; set; }
        public DateTime? DueDate { get; set; }
        

        [Display(Name = "Affected Machine")]
        public string MachineName { get; set; }

        public int WorkOrderNumber { get; set; }
        public string Message { get; set; }

    }
    public class CategoryDropDown
    {
        public string Category { get; set; }
        public string Owner { get; set; }

    }
    public class UrgencyDropDown
    {
        public string Urgency { get; set; }
    }
}