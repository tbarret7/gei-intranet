﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace ExpenseClient.ViewModels
{
    public class EasterEggViewModel
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
}