﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace ExpenseClient.ViewModels
{
    public class OvenViewModel
    {
        [Display(Name = "Cycles for Oven A")]
        public int? CycleNum { get; set; }

        [Display(Name = "Cycles for Oven A")]
        public List<CycleDropDown> cycleDropDownProperty { get; set; }
        public string CycleNumber { get; set; }

        [Display(Name = "Cycles for Oven B")]
        public List<OvenBCycleDropDown> ovenBCycleDropDownProperty { get; set; }

        [Display(Name = "Cycles for Oven B")]
        public int? CycleNumm { get; set; }

        public class CycleDropDown
        {
            public int? cycleNum { get; set; }
        }

        public class OvenBCycleDropDown
        {
            public int? cycleNumm { get; set; }
        }
        

       
    }
}