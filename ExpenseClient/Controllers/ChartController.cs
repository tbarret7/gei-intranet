﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.Data;
using ExpenseClient.Models;
using ExpenseClient.ViewModels;
using static ExpenseClient.ViewModels.OvenViewModel;

namespace ExpenseClient.Controllers
{
    public class ChartController : Controller
    {
        private Press_2_DCEntities _db = new Press_2_DCEntities();

        public List<CycleDropDown> GetDetails() //populate dropdown and model bind
        {
            
            List<CycleDropDown> results = new List<CycleDropDown>();

            using (var _context = new Press_2_DCEntities())
            {
                var obj = _context.tblHeat_Treat_Log
                    .Select(x => x.Cycle_Num_A)
                    .Distinct()
                    .OrderByDescending(x => x);
                    
                if (obj != null && obj.Count() > 0)
                {
                    foreach (var data in obj)
                    {
                        CycleDropDown model = new CycleDropDown();
                        model.cycleNum = data;
                        results.Add(model);
                    }
                }
            }

            return results;
        }

        public List<OvenBCycleDropDown> GetDetailss() //populate dropdown and model bind
        {

            List<OvenBCycleDropDown> results = new List<OvenBCycleDropDown>();
            using (var _context = new Press_2_DCEntities())
            {
                var obj = _context.tblHeat_Treat_Log
                    .Select(x => x.Cycle_Num_B)
                    .Distinct()
                    .OrderByDescending(x => x);

                if (obj != null && obj.Count() > 0)
                {
                    foreach (var data in obj)
                    {
                        OvenBCycleDropDown model = new OvenBCycleDropDown();
                        model.cycleNumm = data;
                        results.Add(model);
                    }
                }
            }
            return results;
        }

        public ActionResult Index()
        {

            return View();
        }
        
        public ActionResult Ovens()
        {
            
            return View();
        }

        public ActionResult OvenCycles()
        {
            OvenViewModel model = new OvenViewModel();
            model.cycleDropDownProperty = GetDetails();
            model.ovenBCycleDropDownProperty = GetDetailss();
            return View(model);
        }

        // Parse Weekendingdate to JSON readable format
        public double weekend(DateTime now)
        {
            DateTime date = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second);
            double time = ((DateTimeOffset)date).ToUnixTimeSeconds() * 1000;
            return time;
        }

        public ContentResult EXTPRESSURE()
        {
            List<DataPoint> dataPoints = new List<DataPoint>();
            dataPoints.Clear();

            foreach (var data in _db.qryEXT_Pressure_Info)
            {
                dataPoints.Add(new DataPoint(weekend(DateTime.Parse(data.Date_Time_Stamp.ToString())), data.EXT_Pressure));
            }

            return Content(JsonConvert.SerializeObject(dataPoints, _jsonSetting), "application/json");
        }

        JsonSerializerSettings _jsonSetting = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };

        public ContentResult OvenAJSON()
        {
            List<Oven> dataPoints = new List<Oven>();
            
            dataPoints.Clear();
           
            foreach (var data in _db.qryHT_Max_A_1_)
            {
                dataPoints.Add(new Oven(1, data.Zone1A));
                dataPoints.Add(new Oven(2, data.Zone2A));

            }

            return Content(JsonConvert.SerializeObject(dataPoints, _jsonSetting), "application/json");

        }

        public ContentResult OvenBJSON()
        {
            List<Oven> dataPoints = new List<Oven>();
            
            dataPoints.Clear();

            foreach (var data in _db.qryHT_Max_B_1_)
            {
                dataPoints.Add(new Oven(1, data.Zone1B));
                dataPoints.Add(new Oven(2, data.Zone2B));
            }

            return Content(JsonConvert.SerializeObject(dataPoints, _jsonSetting), "application/json");
        }

        public ContentResult PRESSEXIT()
        {
            var run = _db.qryEXT_Pressure_Info.Select(x => x.Run_Num).ToList();
            int runNumber = Convert.ToInt32(run[0]);

            List<DataPoint> dataPoints = new List<DataPoint>();
            dataPoints.Clear();            

            foreach (var data in _db.qryEXT_Pressure_Info.Where(m => m.Run_Num == runNumber))
            {
                dataPoints.Add(new DataPoint(weekend(DateTime.Parse(data.Date_Time_Stamp.ToString())), data.Press_Exit));
            }

            return Content(JsonConvert.SerializeObject(dataPoints, _jsonSetting), "application/json");
        }

        public ContentResult CycleZone1A(string input)
        {            
            List<DataPoint> dataPoints = new List<DataPoint>();
            dataPoints.Clear();
            int? cycle = Convert.ToInt32(input);

            foreach (var data in _db.tblHeat_Treat_Log.Where(m => m.Cycle_Num_A == cycle))
            {
                dataPoints.Add(new DataPoint(weekend(DateTime.Parse(data.Date_Time_Stamp.ToString())), data.Zone1A));
            }
                  
            return Content(JsonConvert.SerializeObject(dataPoints, _jsonSetting), "application/json");

        }

        public ContentResult CycleZone2A(string input)
        {
            List<DataPoint> dataPoints = new List<DataPoint>();
            dataPoints.Clear();

            int? cycle = Convert.ToInt32(input);

            foreach (var data1 in _db.tblHeat_Treat_Log.Where(m => m.Cycle_Num_A == cycle))
            {
                dataPoints.Add(new DataPoint(weekend(DateTime.Parse(data1.Date_Time_Stamp.ToString())), data1.Zone2A));
            }

            return Content(JsonConvert.SerializeObject(dataPoints, _jsonSetting), "application/json");
        }

        public ContentResult CycleZone1B(string input)
        {
            List<DataPoint> dataPoints = new List<DataPoint>();
            dataPoints.Clear();

            int? cycle = Convert.ToInt32(input);

            foreach (var data1 in _db.tblHeat_Treat_Log.Where(m => m.Cycle_Num_B == cycle))
            {
                dataPoints.Add(new DataPoint(weekend(DateTime.Parse(data1.Date_Time_Stamp.ToString())), data1.Zone1B));
            }

            return Content(JsonConvert.SerializeObject(dataPoints, _jsonSetting), "application/json");
        }

        public ContentResult CycleZone2B(string input)
        {
            List<DataPoint> dataPoints = new List<DataPoint>();
            dataPoints.Clear();

            int? cycle = Convert.ToInt32(input);

            foreach (var data1 in _db.tblHeat_Treat_Log.Where(m => m.Cycle_Num_B == cycle))
            {
                dataPoints.Add(new DataPoint(weekend(DateTime.Parse(data1.Date_Time_Stamp.ToString())), data1.Zone2B));
            }

            return Content(JsonConvert.SerializeObject(dataPoints, _jsonSetting), "application/json");
        }
    }
}