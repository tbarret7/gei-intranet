﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Mapping;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure.MappingViews;
using System.Threading.Tasks;
using ExpenseClient.ViewModels;
using ExpenseClient.Models;
using System.Net.Mail;
using System.Threading;
using System.Security.Principal;
using System.Security.Authentication;
using System.Security.Permissions;
using System.IO;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using System.Security.Claims;

namespace ExpenseClient.Controllers
{
    public class ISWorkOrderController : Controller
    {
        public List<CategoryDropDown> GetDetails() //populate dropdown and model bind
        {
            GEI_IS_WOEntities db = new GEI_IS_WOEntities();
            List<CategoryDropDown> results = new List<CategoryDropDown>();

            var obj = db.tblCategories.Select(m => m).ToList();
            if (obj != null && obj.Count() > 0)
            {
                foreach (var data in obj)
                {
                    CategoryDropDown model = new CategoryDropDown();
                    model.Category = data.Category;
                    model.Owner = data.Owner;                    
                    results.Add(model);
                }
            }

            return results;
        }
                
        public List<UrgencyDropDown> GetDetail()
        {
            GEI_IS_WOEntities db = new GEI_IS_WOEntities();
            List<UrgencyDropDown> results = new List<UrgencyDropDown>();
            var obj = db.tblImportances.Select(m => m).ToList();
            if (obj != null && obj.Count > 0)
            {
                foreach (var data in obj)
                {
                    UrgencyDropDown model = new UrgencyDropDown();
                    model.Urgency = data.Importance;
                    results.Add(model);
                }
            }

            return results;
        }

        // GET: WorkOrder
        public ActionResult Index()
        {
            string user = User.Identity.Name.Substring(9);
            WorkOrderViewModel model = new WorkOrderViewModel();
            model.MachineName = System.Environment.MachineName;    
            model.CategoryDropDown = GetDetails();
            model.UrgencyDropDown = GetDetail();
            model.Status = "Open";
            model.Date = DateTime.Now;
            model.Username = user;            
            
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(WorkOrderViewModel model)
        {
            string user = User.Identity.Name.Substring(9);
            //Any validation rules

            //Model and property binding
            GEI_IS_WOEntities db = new GEI_IS_WOEntities();
            tblMain dbo = new tblMain();
            model.DueDate = DateTime.Now.AddDays(1);
            model.Username = user;
            CreateCopyMessage("smtp.office365.com", model);            

            dbo.Category = model.category;
            dbo.Date = model.Date;
            dbo.Description = model.Description;
            dbo.Importance = model.Importance;
            dbo.Username = user;
            dbo.Comp_Name = model.MachineName;
            dbo.Status = model.Status;
            dbo.Status_Date = DateTime.Now;
            dbo.Due_Date = DateTime.Now.AddDays(1);

            db.tblMains.Add(dbo);
            db.SaveChanges();
            

            return RedirectToAction("Index"); //return to non-overloaded index
            
        }

        [Authorize(Roles = "WkstAdmin")]
        public ActionResult Admin()
        {
            
            return View();          
            
            
            
            
        }

        [HttpGet]
        public JsonResult LoadData()
        {
            using (GEI_IS_WOEntities dc = new GEI_IS_WOEntities())
            {
                string user = User.Identity.Name.Substring(9);
                WorkOrderViewModel model = new WorkOrderViewModel();
                model.UrgencyDropDown = GetDetail();
                model.CategoryDropDown = GetDetails();
                List<WorkOrderViewModel> List = dc.tblMains
                    .Where(m => m.Status == "Open")
                    .ToList()
                    .Select(m => new WorkOrderViewModel
                    {
                        category = m.Category,
                        Date = m.Date.Value.Date,
                        Username = m.Username,
                        Comp_Name = m.Comp_Name,
                        Importance = m.Importance,
                        Description = m.Description,
                        Status = m.Status,
                        StatusDate = m.Status_Date,
                        DueDate = m.Due_Date,
                        WorkOrderNumber = m.WorkOrderNum
                        
                    }).ToList();

                return Json(List, JsonRequestBehavior.AllowGet);

            }
        }
        
        public ActionResult EditOrder(int wo)
        {
            GEI_IS_WOEntities db = new GEI_IS_WOEntities();
            List<tblMain> list = db.tblMains.ToList();
            ViewBag.DepartmentList = list;

            WorkOrderViewModel model = new WorkOrderViewModel();
            model.CategoryDropDown = GetDetails();
            model.UrgencyDropDown = GetDetail();
            

            tblMain emp = db.tblMains.SingleOrDefault(x => x.WorkOrderNum == wo);
            model.Date = emp.Date;
            model.category = emp.Category;
            model.Username = emp.Username;
            model.MachineName = emp.Comp_Name;
            model.Importance = emp.Importance;
            model.Description = emp.Description;
            model.ISComments = emp.IS_Comments;
            model.Status = emp.Status;
            model.StatusDate = emp.Status_Date;
            model.DueDate = emp.Due_Date;
            model.WorkOrderNumber = emp.WorkOrderNum;

            
            return PartialView("WoPartial", model);
        }

        [HttpPost]
        public ActionResult Edit(WorkOrderViewModel model)
        {
            GEI_IS_WOEntities db = new GEI_IS_WOEntities();
            
                //Update
                var asset = db.tblMains.SingleOrDefault(x => x.WorkOrderNum == model.WorkOrderNumber);
                asset.Date = model.Date;
                asset.Username = model.Username;
                asset.Comp_Name = model.MachineName;
                asset.Category = model.category;
                asset.Importance = model.Importance;
                asset.Description = model.Description;
                asset.IS_Comments = model.ISComments;
                asset.Status = model.Status;
                asset.Status_Date = DateTime.Now.Date;
                asset.WorkOrderNum = model.WorkOrderNumber;
                asset.Due_Date = model.DueDate;
                
                db.SaveChanges();
                CreateResponse("smtp.office365.com", model);

            return RedirectToAction("Index");
            
            
        }

        public static void CreateCopyMessage(string server, WorkOrderViewModel model)
        {
            MailAddress from = new MailAddress("do_not_reply@genext.com", "Alan Bradley");
            MailAddress to = new MailAddress("IS_Dept@genext.com");
            MailMessage message = new MailMessage(from, to);
            // message.Subject = "Using the SmtpClient class.";
            message.IsBodyHtml = true;
            message.Subject = "IS Work Order - " + DateTime.Now.ToShortDateString();
            message.Body = @"<strong>Work Order description:</strong> " + model.Description + "<br />" +
                "<strong>Date submitted:</strong> " + DateTime.Now.ToShortDateString() + "<br />" +
                "<strong>Due Date:</strong> " + model.DueDate + "<br />" +
                "<strong>Category:</strong> " + model.category + "<br />" +
                "<strong>Machine Name:</strong> " + model.MachineName + "<br />" +
                "<strong>Urgency:</strong> " + model.Importance + "<br />" +
                "<strong>Status:</strong> " + model.Status + "<br />" +
                "<strong>Submitted by:</strong> " + model.Username;
            // Add a carbon copy recipient.
            //MailAddress copy = new MailAddress("jdwhaley@genext.com", "John Whaley");
            //message.CC.Add(copy);
            SmtpClient client = new SmtpClient(server);
            // Include credentials if the server requires them.
            //  client.Credentials = CredentialCache.DefaultNetworkCredentials;
            // Console.WriteLine("Sending an email message to {0} by using the SMTP host {1}.",
                //   to.Address, client.Host);

            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void CreateResponse(string server, WorkOrderViewModel model)
        {            

            EmployeeConnection db = new EmployeeConnection();

            tblEmployee emp = db.tblEmployees.SingleOrDefault(x => x.UName == model.Username);
            string path = emp.EMail;

            MailAddress from = new MailAddress("abradley@genext.com", "Alan Bradley"); //change to alan bradley
            MailAddress to = new MailAddress(path, "Submitter");
            MailMessage message = new MailMessage(from, to);
            // message.Subject = "Using the SmtpClient class.";
            message.IsBodyHtml = true;
            message.Subject = "IS Work Order - " + DateTime.Now.ToShortDateString();
            message.Body = @"<strong>Work Order Status:</strong> " + model.Status + "<br />" +
                "<strong>IS Comments: </strong> " + model.ISComments + "<br />" +
                "<strong>If you have any further needs pertaining to this work order, please contact Thomas or John.</strong>";           
               
            // Add a carbon copy recipient.
            //MailAddress copy = new MailAddress("jdwhaley@genext.com", "John Whaley");
            //message.CC.Add(copy);
            SmtpClient client = new SmtpClient(server);
            // Include credentials if the server requires them.
            //  client.Credentials = CredentialCache.DefaultNetworkCredentials;
            // Console.WriteLine("Sending an email message to {0} by using the SMTP host {1}.",
            //   to.Address, client.Host);

            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public static class Security
    {
        public static bool IsInGroup(this ClaimsPrincipal User, string GroupName)
        {
            var groups = new List<string>();

            var wi = (WindowsIdentity)User.Identity;
            if (wi.Groups != null)
            {
                foreach (var group in wi.Groups)
                {
                    try
                    {
                        groups.Add(group.Translate(typeof(NTAccount)).ToString());
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }
                return groups.Contains(GroupName);
            }
            return false;
        }
    }
}