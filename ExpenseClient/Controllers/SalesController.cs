﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExpenseClient.Models;
using ExpenseClient.ViewModels;
using Microsoft.Reporting.WebForms;
using Oracle.ManagedDataAccess.Client;
using static ExpenseClient.ViewModels.SalesViewModel;

namespace ExpenseClient.Controllers
{
    public class SalesController : Controller
    {
        public ActionResult Index()
        {
            SalesViewModel model = new SalesViewModel();
            model.salesDropDownProperty = GetDetails();
            model.PromiseDateBegin = DateTime.Now.AddDays(-1).Date;
            model.PromiseDateEnd = DateTime.Now.AddDays(1).Date;
            return View(model);
        }

        public List<SalesDropDown> GetDetails()
        {
            List<SalesDropDown> results = new List<SalesDropDown>();
            List<string> obj = new List<string>();
            //obj.Add("BOOKED BY CUST ACCTNUM");
            //obj.Add("BOOKED BY CUSTOMER");
            obj.Add("SALES BY CUSTOMER");
            obj.Add("SALES BY CUSTOMER(PRICE PER LB)");
            obj.Add("SALES REPORT BY SALES AMOUNT");
            obj.Add("SALES REPORT BY MONTH FOR SINGLE ACCOUNT");
            obj.Add("SALES REPORT BY DIE FOR SINGLE ACCOUNT");
            obj.Add("SALES REPORT BY ITEM FOR SINGLE ACCOUNT");

            foreach (var data in obj)
            {
                SalesDropDown model = new SalesDropDown();
                model.selected = data;
                results.Add(model);
            }
            
            return results;
        }              

        public ActionResult Router(SalesViewModel model)
        {
            //if (model.report == "BOOKED BY CUST ACCTNUM")
            //{
            //    return RedirectToAction("BookingsByAcctNum", model);
            //}
            //if (model.report == "BOOKED BY CUSTOMER")
            //{
            //    return RedirectToAction("BookingsByCustomer", model);
            //}
            if (model.report == "SALES BY CUSTOMER")
            {
                return RedirectToAction("SalesByCustomer", model);
            }
            if (model.report == "SALES BY CUSTOMER(PRICE PER LB)")
            {
                return RedirectToAction("SalesByCustomerPricePerLb", model);
            }
            if (model.report == "SALES REPORT BY SALES AMOUNT")
            {
                return RedirectToAction("SalesReportBySalesAmount", model);
            }
            if (model.report == "SALES REPORT BY MONTH FOR SINGLE ACCOUNT")
            {
                return RedirectToAction("SalesByAcctByMonth", model);
            }
            if (model.report == "SALES REPORT BY DIE FOR SINGLE ACCOUNT")
            {
                return RedirectToAction("SalesByDieByAcctNum", model);
            }
            if (model.report == "SALES REPORT BY ITEM FOR SINGLE ACCOUNT")
            {
                return RedirectToAction("SalesByItemByAcctNum", model);
            }
            else
            {
               return RedirectToAction("Index");
            }
        }

        //public ActionResult Index(SalesViewModel model) //overload
        //{

        //    return View();
        //}

        public ActionResult SalesReportBySalesAmount(SalesViewModel model)
        {
            ReportViewer rptViewer = new ReportViewer();
            G2ProdDataSet ds = new G2ProdDataSet();
            DateTime beginn = model.PromiseDateBegin.Date;
            DateTime endd = model.PromiseDateEnd.Date;
            string begin = beginn.Date.ToShortDateString();
            string end = endd.Date.ToShortDateString();
            int cxNum = model.CustomerNumber;

            string connectionString = "Data Source=(DESCRIPTION="
                    + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=gei-glovia)(PORT=1521)))"
                    + "(CONNECT_DATA=(SERVER=prod)(SERVICE_NAME=G2prod.gei.local)));"
                    + "User Id=glovia_prod;Password=manager;";

            OracleConnection connection = new OracleConnection(connectionString);

            OracleCommand cmd = new OracleCommand(
            "Select " +
                    "qrySHIPPED_QTY_INV_AMT.OTST_NAME, " +
                    "qrySHIPPED_QTY_INV_AMT.CUSTOMER, " +
                    "ROUND(Sum(qrySHIPPED_QTY_INV_AMT.SumSHIPPEDQTY), 2) As Sum_SHIPPEDQTY, " +
                    "ROUND(Sum(qrySHIPPED_QTY_INV_AMT.SumITEMWEIGHT), 2) As Sum_ITEMWEIGHT, " +
                    "ROUND(Sum(qrySHIPPED_QTY_INV_AMT.SumSALESAMT), 2) As Sum_SALESAMT " +
                "From " +
                    "(Select " +
                         "qrySO_SHIP_GRP.SALES_CCN, " +
                         "qrySO_SHIP_GRP.OTST_NAME, " +
                         "qrySO_SHIP_GRP.AR_DOC, " +
                         "qrySO_SHIP_GRP.SumSHIPPEDQTY, " +
                         "qrySO_SHIP_GRP.SumITEMWEIGHT, " +
                         "qrySO_SHIP_GRP.SumSALESAMT, " +
                         "qryAR_DOC.AMT, " +
                         "qrySO_SHIP_GRP.CUSTOMER, " +
                         "qrySO_SHIP_GRP.USER_ALPHA1, " +
                         "qrySO_SHIP_GRP.Prim_Acct " +
                     "From " +
                          "(Select " +
                               "qrySO_SHIP.SALES_CCN, " +
                               "qrySO_SHIP.OTST_NAME, " +
                               "qrySO_SHIP.AR_DOC, " +
                               "Sum(qrySO_SHIP.SHIPPED_QTY) As SumSHIPPEDQTY, " +
                               "Sum(qrySO_SHIP.ITEM_WEIGHT) As SumITEMWEIGHT, " +
                               "qrySO_SHIP.CUSTOMER, " +
                               "qrySO_SHIP.USER_ALPHA1, " +
                               "qrySO_SHIP.Prim_Acct, " +
                               "Sum(qrySO_SHIP.Sales_Amt) As SumSALESAMT " +
                           "From " +
                                "(Select " +
                                     "SALES_CCN, " +
                                     "SO, " +
                                     "SO_LINE, " +
                                     "AGC, " +
                                     "AGC_CODE, " +
                                     "ITEM, " +
                                     "REVISION, " +
                                     "SELL_UM, " +
                                     "SO_DELIVERY, " +
                                     "SO_SHIPMENT, " +
                                     "INVOICE_DATE, " +
                                     "SHIPPED_QTY, " +
                                     "NET_WEIGHT, " +
                                     "WT, " +
                                     "WT_FT, " +
                                     "TH_WGT, " +
                                     "MAS_LOC, " +
                                     "BOL, " +
                                     "STH_WGT, " +
                                     "TH_WT_PCT, " +
                                     "PCS_NET_WT, " +
                                     "ITEM_NET_WT, " +
                                     "(Case " +
                                         "When SubStr(AGC_CODE, 2) = 'OL' " +
                                         "Then 0 " +
                                         "When SELL_UM = 'MT' " +
                                         "Then ITEM_NET_WT * UM_SCALAR * SHIPPED_QTY " +
                                         "When SELL_UM = 'LB' " +
                                         "Then SHIPPED_QTY " +
                                         "When SELL_UM = 'FT' " +
                                         "Then WT_FT * SHIPPED_QTY " +
                                         "When SELL_UM = 'IN' " +
                                         "Then (WT_FT / 12) * SHIPPED_QTY " +
                                         "Else ITEM_NET_WT * SHIPPED_QTY " +
                                     "End) As ITEM_WEIGHT, " +
                                     "UM_SCALAR, " +
                                     "EXT_AMT As Sales_Amt, " +
                                     "AR_DOC, " +
                                     "CUSTOMER, " +
                                     "OTST_NAME, " +
                                     "USER_ALPHA1, " +
                                     "CUS_LOC, " +
                                     "(Case " +
                                         "When USER_ALPHA1 = ' ' " +
                                         "Then CUSTOMER " +
                                         "Else USER_ALPHA1 " +
                                     "End) As Prim_Acct " +
                                 "From " +
                                      "(Select " +
                                           "SALES_CCN, " +
                                           "SO, " +
                                           "SO_LINE, " +
                                           "AGC, " +
                                           "AGC_CODE, " +
                                           "ITEM, " +
                                           "REVISION, " +
                                           "Die, " +
                                           "SELL_UM, " +
                                           "SO_DELIVERY, " +
                                           "SO_SHIPMENT, " +
                                           "INVOICE_DATE, " +
                                           "SHIPPED_QTY, " +
                                           "NET_WEIGHT, " +
                                           "WT, " +
                                           "WT_FT, " +
                                           "TH_WGT, " +
                                           "BOL, " +
                                           "STH_WGT, " +
                                           "TH_WT_PCT, " +
                                           "PCS_NET_WT, " +
                                           "(Case " +
                                               "When SELL_UM = 'EA' " +
                                               "Then (PCS_NET_WT / SHIPPED_QTY) / UM_SCALAR " +
                                           "End) As ITEM_NET_WT, " +
                                           "UM_SCALAR, " +
                                           "EXT_AMT, " +
                                           "AR_DOC, " +
                                           "CUSTOMER, " +
                                           "OTST_NAME, " +
                                           "USER_ALPHA1, " +
                                           "MAS_LOC, " +
                                           "CUS_LOC " +
                                       "From " +
                                            "(Select " +
                                                 "SALES_CCN, " +
                                                 "SO, " +
                                                 "SO_LINE, " +
                                                 "AGC, " +
                                                 "AGC_CODE, " +
                                                 "ITEM, " +
                                                 "REVISION, " +
                                                 "Die, " +
                                                 "SELL_UM, " +
                                                 "SO_DELIVERY, " +
                                                 "SO_SHIPMENT, " +
                                                 "INVOICE_DATE, " +
                                                 "SHIPPED_QTY, " +
                                                 "NET_WEIGHT, " +
                                                 "WT, " +
                                                 "WT_FT, " +
                                                 "TH_WGT, " +
                                                 "BOL, " +
                                                 "STH_WGT, " +
                                                 "(Case " +
                                                     "When STH_WGT Is Null " +
                                                     "Then 0 " +
                                                     "Else (TH_WGT / STH_WGT) " +
                                                 "End) As TH_WT_PCT, " +
                                                 "(Case " +
                                                     "When STH_WGT Is Null " +
                                                     "Then 0 " +
                                                     "Else NET_WEIGHT * (TH_WGT / STH_WGT) " +
                                                 "End) As PCS_NET_WT, " +
                                                 "UM_SCALAR, " +
                                                 "EXT_AMT, " +
                                                 "AR_DOC, " +
                                                 "CUSTOMER, " +
                                                 "OTST_NAME, " +
                                                 "USER_ALPHA1, " +
                                                 "MAS_LOC, " +
                                                 "CUS_LOC " +
                                             "From " +
                                                  "(Select " +
                                                       "SALES_CCN, " +
                                                       "SO, " +
                                                       "SO_LINE, " +
                                                       "AGC, " +
                                                       "AGC_CODE, " +
                                                       "ITEM, " +
                                                       "REVISION, " +
                                                       "Die, " +
                                                       "SO_DELIVERY, " +
                                                       "SO_SHIPMENT, " +
                                                       "INVOICE_DATE, " +
                                                       "SHIPPED_QTY, " +
                                                       "NET_WEIGHT, " +
                                                       "WT, " +
                                                       "WT_FT, " +
                                                       "BOL, " +
                                                       "SELL_UM, " +
                                                       "UM_SCALAR, " +
                                                       "(Case " +
                                                           "When SELL_UM = 'MT' " +
                                                           "Then WT * UM_SCALAR * SHIPPED_QTY " +
                                                           "When SELL_UM = 'LB' " +
                                                           "Then SHIPPED_QTY " +
                                                           "When SELL_UM = 'FT' " +
                                                           "Then WT_FT * SHIPPED_QTY " +
                                                           "When SELL_UM = 'IN' " +
                                                           "Then (WT_FT / 12) * SHIPPED_QTY " +
                                                           "Else WT * SHIPPED_QTY " +
                                                       "End) As TH_WGT, " +
                                                       "STH_WGT, " +
                                                       "AR_DOC, " +
                                                       "EXT_AMT, " +
                                                       "CUSTOMER, " +
                                                       "OTST_NAME, " +
                                                       "USER_ALPHA1, " +
                                                       "MAS_LOC, " +
                                                       "CUS_LOC " +
                                                   "From " +
                                                        "(Select " +
                                                             "SO.SALES_CCN, " +
                                                             "SO.SO, " +
                                                             "SO.SO_LINE, " +
                                                             "SO.AGC, " +
                                                             "(Case " +
                                                                 "When qryITEM_INFO.AGC Is Null " +
                                                                 "Then SO.AGC " +
                                                                 "Else qryITEM_INFO.AGC " +
                                                             "End) As AGC_CODE, " +
                                                             "SO.ITEM, " +
                                                             "SO.REVISION, " +
                                                             "qryITEM_INFO.Die, " +
                                                             "SO.SELL_UM, " +
                                                             "SO_DEL.SO_DELIVERY, " +
                                                             "qrySO_SHIP_DTRNG.SO_SHIPMENT, " +
                                                             "qrySO_SHIP_DTRNG.INVOICE_DATE, " +
                                                             "qrySO_SHIP_DTRNG.QTY As SHIPPED_QTY, " +
                                                             "BOL_HDR.NET_WEIGHT, " +
                                                             "(Case " +
                                                                 "When qryITEM_INFO.WEIGHT Is Null " +
                                                                 "Then 0 " +
                                                                 "Else qryITEM_INFO.WEIGHT " +
                                                             "End) As WT, " +
                                                             "(Case " +
                                                                 "When qryITEM_INFO.USER1 Is Null " +
                                                                 "Then '0' " +
                                                                 "Else qryITEM_INFO.USER1 " +
                                                             "End) As WT_FT, " +
                                                             "BOL_HDR.BOL, " +
                                                             "qryBOL_TOTALS.STH_WGT, " +
                                                             "SO.UM_SCALAR, " +
                                                             "SO.UNIT_PRICE, " +
                                                             "qrySO_SHIP_DTRNG.AR_DOC, " +
                                                             "qrySO_SHIP_DTRNG.EXT_AMT, " +
                                                             "SO_HDR.CUSTOMER, " +
                                                             "CUS_LOC.NAME As OTST_NAME, " +
                                                             "CUS_LOC.USER_ALPHA1, " +
                                                             "BOL_HDR.MAS_LOC, " +
                                                             "CUS_LOC.CUS_LOC " +
                                                         "From " +
                                                             "BOL_HDR Inner Join " +
                                                              "(Select " +
                                                                   "* " +
                                                               "From " +
                                                                   "SO_SHIP " +
                                                               "Where " +
                                                                   "SO_SHIP.SALES_CCN = 7 And " +
                                                                   "SO_SHIP.INVOICE_DATE Between To_Date('" + begin + "', 'mm/dd/yy') And " +
                                                                   "To_Date('" + end + "', 'mm/dd/yy')) qrySO_SHIP_DTRNG On " +
                                                                     "BOL_HDR.CCN = qrySO_SHIP_DTRNG.SALES_CCN " +
                                                                     "And BOL_HDR.MAS_LOC = qrySO_SHIP_DTRNG.MAS_LOC " +
                                                                     "And BOL_HDR.PACKLIST = qrySO_SHIP_DTRNG.PACKLIST Inner Join " +
                                                             "SO_DEL On qrySO_SHIP_DTRNG.SALES_CCN = SO_DEL.SALES_CCN " +
                                                                     "And qrySO_SHIP_DTRNG.SO = SO_DEL.SO " +
                                                                     "And qrySO_SHIP_DTRNG.SO_LINE = SO_DEL.SO_LINE " +
                                                                     "And qrySO_SHIP_DTRNG.SO_DELIVERY = SO_DEL.SO_DELIVERY Inner Join " +
                                                             "SO On SO_DEL.SALES_CCN = SO.SALES_CCN " +
                                                                     "And SO_DEL.SO = SO.SO " +
                                                                     "And SO_DEL.SO_LINE = SO.SO_LINE Inner Join " +
                                                             "SO_HDR On SO_HDR.SALES_CCN = SO.SALES_CCN " +
                                                                     "And SO_HDR.SO = SO.SO Left Join " +
                                                             "CUS_LOC On CUS_LOC.CUSTOMER = SO_HDR.CUSTOMER " +
                                                                     "And CUS_LOC.CUS_LOC = SO_HDR.CUS_AR_LOC Left Join " +
                                                              "(Select " +
                                                                   "ITEM_CCN.CCN, " +
                                                                   "ITEM_CCN.ITEM, " +
                                                                   "ITEM_CCN.REVISION, " +
                                                                   "ITEM_CCN.AGC, " +
                                                                   "ITEM.LENGTH, " +
                                                                   "ITEM.USER1, " +
                                                                   "ITEM.WEIGHT, " +
                                                                   "ITEM_CCN.USER2 As Die " +
                                                               "From " +
                                                                   "ITEM Inner Join " +
                                                                   "ITEM_CCN On ITEM_CCN.ITEM = ITEM.ITEM " +
                                                                           "And ITEM_CCN.REVISION = ITEM.REVISION) qryITEM_INFO " +
                                                                 "On qryITEM_INFO.CCN = SO.SALES_CCN " +
                                                                     "And qryITEM_INFO.ITEM = SO.ITEM " +
                                                                     "And qryITEM_INFO.REVISION = SO.REVISION Inner Join " +
                                                              "(Select " +
                                                                   "qryITEM_WEIGHTS.SALES_CCN, " +
                                                                   "qryITEM_WEIGHTS.MAS_LOC, " +
                                                                   "qryITEM_WEIGHTS.PACKLIST, " +
                                                                   "qryITEM_WEIGHTS.BOL, " +
                                                                   "Sum(qryITEM_WEIGHTS.TH_WGT) As STH_WGT " +
                                                               "From " +
                                                                    "(Select " +
                                                                         "SALES_CCN, " +
                                                                         "BOL, " +
                                                                         "MAS_LOC, " +
                                                                         "PACKLIST, " +
                                                                         "ITEM, " +
                                                                         "REVISION, " +
                                                                         "QTY, " +
                                                                         "WT, " +
                                                                         "WT_FT, " +
                                                                         "UM_SCALAR, " +
                                                                         "(Case " +
                                                                             "When SELL_UM = 'MT' " +
                                                                             "Then WT * UM_SCALAR * QTY " +
                                                                             "When SELL_UM = 'LB' " +
                                                                             "Then QTY " +
                                                                             "When SELL_UM = 'FT' " +
                                                                             "Then WT_FT * QTY " +
                                                                             "When SELL_UM = 'IN' " +
                                                                             "Then (WT_FT / 12) * QTY " +
                                                                             "Else WT * QTY " +
                                                                         "End) As TH_WGT " +
                                                                     "From " +
                                                                          "(Select " +
                                                                               "SO.SALES_CCN, " +
                                                                               "SO_SHIP.BOL, " +
                                                                               "SO_SHIP.MAS_LOC, " +
                                                                               "SO_SHIP.PACKLIST, " +
                                                                               "SO.ITEM, " +
                                                                               "SO.REVISION, " +
                                                                               "SO_SHIP.QTY, " +
                                                                               "SO.UM_SCALAR, " +
                                                                               "SO.SELL_UM, " +
                                                                               "(Case " +
                                                                                   "When ITEM.WEIGHT Is Null " +
                                                                                   "Then 0 " +
                                                                                   "Else ITEM.WEIGHT " +
                                                                               "End) As WT, " +
                                                                               "(Case " +
                                                                                   "When ITEM.USER1 Is Null " +
                                                                                   "Then '0' " +
                                                                                   "Else ITEM.USER1 " +
                                                                               "End) As WT_FT " +
                                                                           "From " +
                                                                               "SO Inner Join " +
                                                                               "SO_DEL On SO_DEL.SALES_CCN = SO.SALES_CCN " +
                                                                                       "And SO_DEL.SO = SO.SO " +
                                                                                       "And SO_DEL.SO_LINE = SO.SO_LINE Inner Join " +
                                                                               "SO_SHIP On SO_SHIP.SALES_CCN = SO_DEL.SALES_CCN " +
                                                                                       "And SO_SHIP.SO = SO_DEL.SO " +
                                                                                       "And SO_SHIP.SO_LINE = SO_DEL.SO_LINE " +
                                                                                       "And SO_SHIP.SO_DELIVERY = SO_DEL.SO_DELIVERY " +
                                                                               "Left Join " +
                                                                               "ITEM On ITEM.ITEM = SO.ITEM " +
                                                                                       "And ITEM.REVISION = SO.REVISION " +
                                                                           "Where " +
                                                                               "SO.SALES_CCN = 7)) qryITEM_WEIGHTS " +
                                                               "Group By " +
                                                                   "qryITEM_WEIGHTS.SALES_CCN, " +
                                                                   "qryITEM_WEIGHTS.MAS_LOC, " +
                                                                   "qryITEM_WEIGHTS.PACKLIST, " +
                                                                   "qryITEM_WEIGHTS.BOL) qryBOL_TOTALS On qryBOL_TOTALS.SALES_CCN = " +
                                                                     "BOL_HDR.CCN " +
                                                                     "And qryBOL_TOTALS.MAS_LOC = BOL_HDR.MAS_LOC " +
                                                                     "And qryBOL_TOTALS.PACKLIST = BOL_HDR.PACKLIST " +
                                                                     "And qryBOL_TOTALS.BOL = BOL_HDR.BOL)))) " +
                                 "Where " +
                                     "SALES_CCN = 7) qrySO_SHIP " +
                           "Where " +
                               "qrySO_SHIP.SALES_CCN = 7 " +
                           "Group By " +
                               "qrySO_SHIP.SALES_CCN, " +
                               "qrySO_SHIP.OTST_NAME, " +
                               "qrySO_SHIP.AR_DOC, " +
                               "qrySO_SHIP.CUSTOMER, " +
                               "qrySO_SHIP.USER_ALPHA1, " +
                               "qrySO_SHIP.Prim_Acct " +
                           "Order By " +
                               "qrySO_SHIP.OTST_NAME) qrySO_SHIP_GRP Inner Join " +
                          "(Select " +
                               "AR_DOC.AR_CCN, " +
                               "AR_DOC.AR_DOC_TYPE, " +
                               "AR_DOC.AR_DOC, " +
                               "AR_DOC.CCN, " +
                               "AR_DOC.AGC, " +
                               "AR_DOC.AMT, " +
                               "AR_DOC.CM_APPLIED_AMT, " +
                               "AR_DOC.BOOK_RATE, " +
                               "AR_DOC.CREATED_DATE, " +
                               "AR_DOC.CURRENCY, " +
                               "AR_DOC.CUS_PO, " +
                               "AR_DOC.DT_, " +
                               "AR_DOC.DISC_AMT, " +
                               "AR_DOC.DISC_APPLIED_AMT, " +
                               "AR_DOC.DISC_DUE_DATE, " +
                               "AR_DOC.DISC_EXCEPTION_AMT, " +
                               "AR_DOC.DISC_GRACE_DAYS, " +
                               "AR_DOC.DISC_PCT, " +
                               "AR_DOC.FROZEN_DATE, " +
                               "AR_DOC.NET_DUE_DATE, " +
                               "AR_DOC.PAID_AMT, " +
                               "AR_DOC.PAID_DATE, " +
                               "AR_DOC.ORIG_BOOK_RATE, " +
                               "AR_DOC.REASON, " +
                               "AR_DOC.REVAL_DATE, " +
                               "AR_DOC.SOURCE, " +
                               "AR_DOC.STMT_DATE, " +
                               "AR_DOC.TERMS_CODE, " +
                               "AR_DOC.VAT_AMT, " +
                               "AR_DOC.VAT_BASE_AMT, " +
                               "AR_DOC.VAT_REG, " +
                               "AR_DOC.WRITE_OFF_AMT, " +
                               "AR_DOC.WRITE_OFF_REASON, " +
                               "AR_DOC.NOTES_REC, " +
                               "AR_DOC.DISC_OVERRIDDEN, " +
                               "AR_DOC.VAT_EXEMPT, " +
                               "AR_DOC.SCHED_PMT, " +
                               "AR_DOC.LAST_VAR_SEQ, " +
                               "AR_DOC.LAST_PMT_SEQ, " +
                               "AR_DOC.BANK, " +
                               "AR_DOC.AR_DEP_SEQ, " +
                               "AR_DOC.AR_DEP_LINE, " +
                               "AR_DOC.UNAPPL_CASH_MEMO, " +
                               "AR_DOC.BOOK_RATE_DIV, " +
                               "AR_DOC.ORIG_BOOK_RATE_DIV, " +
                               "AR_DOC.VAT_EXEMPT_REASON, " +
                               "AR_DOC.CUSTOMER, " +
                               "AR_DOC.CUS_AR_LOC, " +
                               "AR_DOC.AR_DEP_DATE, " +
                               "AR_DOC.NET_DUE_ADATE, " +
                               "AR_DOC.WRITEOFF_APPROVER_NAME, " +
                               "AR_DOC.CONTRA_AMT, " +
                               "AR_DOC.CONTRA_NO, " +
                               "AR_DOC.AI, " +
                               "AR_DOC.DISPUTED, " +
                               "AR_DOC.REVERSE_DATE, " +
                               "AR_DOC.REVERSE_REASON, " +
                               "AR_DOC.REVERSE_MEMO, " +
                               "AR_DOC.PRINT_NOTES, " +
                               "AR_DOC.REVERSE_ASOF_DATE, " +
                               "AR_DOC.WRITE_OFF_ACCT, " +
                               "AR_DOC.TRANSACTION_TYPE_CODE, " +
                               "AR_DOC.VATPCD_CODE, " +
                               "AR_DOC.DISPATCH_VAT_REG, " +
                               "AR_DOC.LAST_ASOF_DATE, " +
                               "AR_DOC.LAST_TRANS_RATE, " +
                               "AR_DOC.LAST_TRANS_RATE_DATE, " +
                               "AR_DOC.LAST_TRANS_RATE_DIV, " +
                               "AR_DOC.LAST_TRANS_TYPE, " +
                               "AR_DOC.LAST_APPLIED_DATE, " +
                               "AR_DOC.USER_ALPHA1, " +
                               "AR_DOC.USER_ALPHA2, " +
                               "AR_DOC.USER_ALPHA3, " +
                               "AR_DOC.USER_NUM1, " +
                               "AR_DOC.USER_NUM2, " +
                               "AR_DOC.USER_NUM3, " +
                               "AR_DOC.USER_DATE, " +
                               "AR_DOC.USER_TIME, " +
                               "AR_DOC.IN_PAYMENT, " +
                               "AR_DOC.REFUND_AMT, " +
                               "AR_DOC.REFUND_DATE, " +
                               "AR_DOC.SEL_PMT_AMT, " +
                               "AR_DOC.AR_CRDET_SEQ, " +
                               "AR_DOC.BASE_DISC_AMT, " +
                               "AR_DOC.BASE_PAYMENT_AMT, " +
                               "AR_DOC.CHG_E7_AMT, " +
                               "AR_DOC.E7_AMT, " +
                               "AR_DOC.LAST_APPLIED_ACDET_SEQ, " +
                               "AR_DOC.LAST_APPLIED_DOC, " +
                               "AR_DOC.LAST_APPLIED_DOC_TYPE, " +
                               "AR_DOC.LAST_WOFF_SEQ " +
                           "From " +
                               "AR_DOC " +
                           "Where " +
                               "AR_DOC.CCN = 7 And " +
                               "AR_DOC.AR_DOC_TYPE = 'I' And " +
                               "AR_DOC.DT_ Between To_Date('" + begin + "', 'mm/dd/yy') And To_Date('" + end + "', 'mm/dd/yy')) qryAR_DOC On " +
                                 "qryAR_DOC.AR_CCN = qrySO_SHIP_GRP.SALES_CCN " +
                                 "And qryAR_DOC.AR_DOC = qrySO_SHIP_GRP.AR_DOC) qrySHIPPED_QTY_INV_AMT " +
                "Group By " +
                    "qrySHIPPED_QTY_INV_AMT.OTST_NAME, " +
                    "qrySHIPPED_QTY_INV_AMT.CUSTOMER " +
                "Order By " +
                    "Sum_SALESAMT Desc", connection);

            OracleDataAdapter adapter = new OracleDataAdapter(cmd);

            connection.Open();
            adapter.Fill(ds, ds.Tables[7].TableName);
            adapter.Dispose();
            connection.Close();
            connection.Dispose();

            foreach (DataRow row in ds.SalesReportBySalesAmount)
            {
                row[5] = begin;
                row[6] = end;
            }

            ds.AcceptChanges();

            // ProcessingMode will be Either Remote or Local  
            rptViewer.ProcessingMode = ProcessingMode.Local;
            rptViewer.SizeToReportContent = true;
            rptViewer.ZoomMode = ZoomMode.FullPage;
            rptViewer.AsyncRendering = false;
            rptViewer.LocalReport.ReportPath = @"C:\Users\tbarrett\source\repos\ExpenseClient\ExpenseClient\Reports\SalesReportBySalesAmount.rdlc";
            ReportDataSource src = new ReportDataSource("SalesReportBySalesAmount", ds.Tables[7]); //@"C:\inetpub\wwwroot\Intranet\Reports\BookingsByAcctNum.rdlc";
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(src);

            ViewBag.ReportViewer = rptViewer;

            return View();
        }

        public ActionResult SalesByCustomerPricePerLb(SalesViewModel model)
        {
            ReportViewer rptViewer = new ReportViewer();
            G2ProdDataSet ds = new G2ProdDataSet();
            DateTime beginn = model.PromiseDateBegin.Date;
            DateTime endd = model.PromiseDateEnd.Date;
            string begin = beginn.Date.ToShortDateString();
            string end = endd.Date.ToShortDateString();
            int cxNum = model.CustomerNumber;

            string connectionString = "Data Source=(DESCRIPTION="
                    + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=gei-glovia)(PORT=1521)))"
                    + "(CONNECT_DATA=(SERVER=prod)(SERVICE_NAME=G2prod.gei.local)));"
                    + "User Id=glovia_prod;Password=manager;";

            OracleConnection connection = new OracleConnection(connectionString);

            OracleCommand cmd = new OracleCommand(
                "Select " +
                    "OTST_NAME, " +
                    "CUSTOMER, " +
                    "Sum_SHIPPEDQTY, " +
                    "Sum_ITEMWEIGHT, " +
                    "Sum_SALESAMT, " +
                    "ROUND((Case " +
                    "When Sum_ITEMWEIGHT = 0 " +
                    "Then 0 " +
                    "Else (Sum_SALESAMT / Sum_ITEMWEIGHT) " +
                    "End), 2) As PricePerLb " +
                "From " +
                "(Select " +
                    "qrySHIPPED_QTY_INV_AMT.OTST_NAME, " +
                    "qrySHIPPED_QTY_INV_AMT.CUSTOMER, " +
                    "ROUND(Sum(qrySHIPPED_QTY_INV_AMT.SumSHIPPEDQTY), 2) As Sum_SHIPPEDQTY, " +
                    "ROUND(Sum(qrySHIPPED_QTY_INV_AMT.SumITEMWEIGHT), 2) As Sum_ITEMWEIGHT, " +
                    "ROUND(Sum(qrySHIPPED_QTY_INV_AMT.SumSALESAMT), 2) As Sum_SALESAMT " +
                "From " +
                    "(Select " +
                         "qrySO_SHIP_GRP.SALES_CCN, " +
                         "qrySO_SHIP_GRP.OTST_NAME, " +
                         "qrySO_SHIP_GRP.AR_DOC, " +
                         "qrySO_SHIP_GRP.SumSHIPPEDQTY, " +
                         "qrySO_SHIP_GRP.SumITEMWEIGHT, " +
                         "qrySO_SHIP_GRP.SumSALESAMT, " +
                         "qryAR_DOC.AMT, " +
                         "qrySO_SHIP_GRP.CUSTOMER, " +
                         "qrySO_SHIP_GRP.USER_ALPHA1, " +
                         "qrySO_SHIP_GRP.Prim_Acct " +
                     "From " +
                          "(Select " +
                               "qrySO_SHIP.SALES_CCN, " +
                               "qrySO_SHIP.OTST_NAME, " +
                               "qrySO_SHIP.AR_DOC, " +
                               "Sum(qrySO_SHIP.SHIPPED_QTY) As SumSHIPPEDQTY, " +
                               "Sum(qrySO_SHIP.ITEM_WEIGHT) As SumITEMWEIGHT, " +
                               "qrySO_SHIP.CUSTOMER, " +
                               "qrySO_SHIP.USER_ALPHA1, " +
                               "qrySO_SHIP.Prim_Acct, " +
                               "Sum(qrySO_SHIP.Sales_Amt) As SumSALESAMT " +
                           "From " +
                                "(Select " +
                                     "SALES_CCN, " +
                                     "SO, " +
                                     "SO_LINE, " +
                                     "AGC, " +
                                     "AGC_CODE, " +
                                     "ITEM, " +
                                     "REVISION, " +
                                     "SELL_UM, " +
                                     "SO_DELIVERY, " +
                                     "SO_SHIPMENT, " +
                                     "INVOICE_DATE, " +
                                     "SHIPPED_QTY, " +
                                     "NET_WEIGHT, " +
                                     "WT, " +
                                     "WT_FT, " +
                                     "TH_WGT, " +
                                     "MAS_LOC, " +
                                     "BOL, " +
                                     "STH_WGT, " +
                                     "TH_WT_PCT, " +
                                     "PCS_NET_WT, " +
                                     "ITEM_NET_WT, " +
                                     "(Case " +
                                         "When SubStr(AGC_CODE, 2) = 'OL' " +
                                         "Then 0 " +
                                         "When SELL_UM = 'MT' " +
                                         "Then ITEM_NET_WT * UM_SCALAR * SHIPPED_QTY " +
                                         "When SELL_UM = 'LB' " +
                                         "Then SHIPPED_QTY " +
                                         "When SELL_UM = 'FT' " +
                                         "Then WT_FT * SHIPPED_QTY " +
                                         "When SELL_UM = 'IN' " +
                                         "Then (WT_FT / 12) * SHIPPED_QTY " +
                                         "Else ITEM_NET_WT * SHIPPED_QTY " +
                                     "End) As ITEM_WEIGHT, " +
                                     "UM_SCALAR, " +
                                     "EXT_AMT As Sales_Amt, " +
                                     "AR_DOC, " +
                                     "CUSTOMER, " +
                                     "OTST_NAME, " +
                                     "USER_ALPHA1, " +
                                     "CUS_LOC, " +
                                     "(Case " +
                                         "When USER_ALPHA1 = ' ' " +
                                         "Then CUSTOMER " +
                                         "Else USER_ALPHA1 " +
                                     "End) As Prim_Acct " +
                                 "From " +
                                      "(Select " +
                                           "SALES_CCN, " +
                                           "SO, " +
                                           "SO_LINE, " +
                                           "AGC, " +
                                           "AGC_CODE, " +
                                           "ITEM, " +
                                           "REVISION, " +
                                           "Die, " +
                                           "SELL_UM, " +
                                           "SO_DELIVERY, " +
                                           "SO_SHIPMENT, " +
                                           "INVOICE_DATE, " +
                                           "SHIPPED_QTY, " +
                                           "NET_WEIGHT, " +
                                           "WT, " +
                                           "WT_FT, " +
                                           "TH_WGT, " +
                                           "BOL, " +
                                           "STH_WGT, " +
                                           "TH_WT_PCT, " +
                                           "PCS_NET_WT, " +
                                           "(Case " +
                                               "When SELL_UM = 'EA' " +
                                               "Then (PCS_NET_WT / SHIPPED_QTY) / UM_SCALAR " +
                                           "End) As ITEM_NET_WT, " +
                                           "UM_SCALAR, " +
                                           "EXT_AMT, " +
                                           "AR_DOC, " +
                                           "CUSTOMER, " +
                                           "OTST_NAME, " +
                                           "USER_ALPHA1, " +
                                           "MAS_LOC, " +
                                           "CUS_LOC " +
                                       "From " +
                                            "(Select " +
                                                 "SALES_CCN, " +
                                                 "SO, " +
                                                 "SO_LINE, " +
                                                 "AGC, " +
                                                 "AGC_CODE, " +
                                                 "ITEM, " +
                                                 "REVISION, " +
                                                 "Die, " +
                                                 "SELL_UM, " +
                                                 "SO_DELIVERY, " +
                                                 "SO_SHIPMENT, " +
                                                 "INVOICE_DATE, " +
                                                 "SHIPPED_QTY, " +
                                                 "NET_WEIGHT, " +
                                                 "WT, " +
                                                 "WT_FT, " +
                                                 "TH_WGT, " +
                                                 "BOL, " +
                                                 "STH_WGT, " +
                                                 "(Case " +
                                                     "When STH_WGT Is Null " +
                                                     "Then 0 " +
                                                     "Else (TH_WGT / STH_WGT) " +
                                                 "End) As TH_WT_PCT, " +
                                                 "(Case " +
                                                     "When STH_WGT Is Null " +
                                                     "Then 0 " +
                                                     "Else NET_WEIGHT * (TH_WGT / STH_WGT) " +
                                                 "End) As PCS_NET_WT, " +
                                                 "UM_SCALAR, " +
                                                 "EXT_AMT, " +
                                                 "AR_DOC, " +
                                                 "CUSTOMER, " +
                                                 "OTST_NAME, " +
                                                 "USER_ALPHA1, " +
                                                 "MAS_LOC, " +
                                                 "CUS_LOC " +
                                             "From " +
                                                  "(Select " +
                                                       "SALES_CCN, " +
                                                       "SO, " +
                                                       "SO_LINE, " +
                                                       "AGC, " +
                                                       "AGC_CODE, " +
                                                       "ITEM, " +
                                                       "REVISION, " +
                                                       "Die, " +
                                                       "SO_DELIVERY, " +
                                                       "SO_SHIPMENT, " +
                                                       "INVOICE_DATE, " +
                                                       "SHIPPED_QTY, " +
                                                       "NET_WEIGHT, " +
                                                       "WT, " +
                                                       "WT_FT, " +
                                                       "BOL, " +
                                                       "SELL_UM, " +
                                                       "UM_SCALAR, " +
                                                       "(Case " +
                                                           "When SELL_UM = 'MT' " +
                                                           "Then WT * UM_SCALAR * SHIPPED_QTY " +
                                                           "When SELL_UM = 'LB' " +
                                                           "Then SHIPPED_QTY " +
                                                           "When SELL_UM = 'FT' " +
                                                           "Then WT_FT * SHIPPED_QTY " +
                                                           "When SELL_UM = 'IN' " +
                                                           "Then (WT_FT / 12) * SHIPPED_QTY " +
                                                           "Else WT * SHIPPED_QTY " +
                                                       "End) As TH_WGT, " +
                                                       "STH_WGT, " +
                                                       "AR_DOC, " +
                                                       "EXT_AMT, " +
                                                       "CUSTOMER, " +
                                                       "OTST_NAME, " +
                                                       "USER_ALPHA1, " +
                                                       "MAS_LOC, " +
                                                       "CUS_LOC " +
                                                   "From " +
                                                        "(Select " +
                                                             "SO.SALES_CCN, " +
                                                             "SO.SO, " +
                                                             "SO.SO_LINE, " +
                                                             "SO.AGC, " +
                                                             "(Case " +
                                                                 "When qryITEM_INFO.AGC Is Null " +
                                                                 "Then SO.AGC " +
                                                                 "Else qryITEM_INFO.AGC " +
                                                             "End) As AGC_CODE, " +
                                                             "SO.ITEM, " +
                                                             "SO.REVISION, " +
                                                             "qryITEM_INFO.Die, " +
                                                             "SO.SELL_UM, " +
                                                             "SO_DEL.SO_DELIVERY, " +
                                                             "qrySO_SHIP_DTRNG.SO_SHIPMENT, " +
                                                             "qrySO_SHIP_DTRNG.INVOICE_DATE, " +
                                                             "qrySO_SHIP_DTRNG.QTY As SHIPPED_QTY, " +
                                                             "BOL_HDR.NET_WEIGHT, " +
                                                             "(Case " +
                                                                 "When qryITEM_INFO.WEIGHT Is Null " +
                                                                 "Then 0 " +
                                                                 "Else qryITEM_INFO.WEIGHT " +
                                                             "End) As WT, " +
                                                             "(Case " +
                                                                 "When qryITEM_INFO.USER1 Is Null " +
                                                                 "Then '0' " +
                                                                 "Else qryITEM_INFO.USER1 " +
                                                             "End) As WT_FT, " +
                                                             "BOL_HDR.BOL, " +
                                                             "qryBOL_TOTALS.STH_WGT, " +
                                                             "SO.UM_SCALAR, " +
                                                             "SO.UNIT_PRICE, " +
                                                             "qrySO_SHIP_DTRNG.AR_DOC, " +
                                                             "qrySO_SHIP_DTRNG.EXT_AMT, " +
                                                             "SO_HDR.CUSTOMER, " +
                                                             "CUS_LOC.NAME As OTST_NAME, " +
                                                             "CUS_LOC.USER_ALPHA1, " +
                                                             "BOL_HDR.MAS_LOC, " +
                                                             "CUS_LOC.CUS_LOC " +
                                                         "From " +
                                                             "BOL_HDR Inner Join " +
                                                              "(Select " +
                                                                   "* " +
                                                               "From " +
                                                                   "SO_SHIP " +
                                                               "Where " +
                                                                   "SO_SHIP.SALES_CCN = 7 And " +
                                                                   "SO_SHIP.INVOICE_DATE Between To_Date('" + begin + "', 'mm/dd/yy') And " +
                                                                   "To_Date('" + end + "', 'mm/dd/yy')) qrySO_SHIP_DTRNG On " +
                                                                     "BOL_HDR.CCN = qrySO_SHIP_DTRNG.SALES_CCN " +
                                                                     "And BOL_HDR.MAS_LOC = qrySO_SHIP_DTRNG.MAS_LOC " +
                                                                     "And BOL_HDR.PACKLIST = qrySO_SHIP_DTRNG.PACKLIST Inner Join " +
                                                             "SO_DEL On qrySO_SHIP_DTRNG.SALES_CCN = SO_DEL.SALES_CCN " +
                                                                     "And qrySO_SHIP_DTRNG.SO = SO_DEL.SO " +
                                                                     "And qrySO_SHIP_DTRNG.SO_LINE = SO_DEL.SO_LINE " +
                                                                     "And qrySO_SHIP_DTRNG.SO_DELIVERY = SO_DEL.SO_DELIVERY Inner Join " +
                                                             "SO On SO_DEL.SALES_CCN = SO.SALES_CCN " +
                                                                     "And SO_DEL.SO = SO.SO " +
                                                                     "And SO_DEL.SO_LINE = SO.SO_LINE Inner Join " +
                                                             "SO_HDR On SO_HDR.SALES_CCN = SO.SALES_CCN " +
                                                                     "And SO_HDR.SO = SO.SO Left Join " +
                                                             "CUS_LOC On CUS_LOC.CUSTOMER = SO_HDR.CUSTOMER " +
                                                                     "And CUS_LOC.CUS_LOC = SO_HDR.CUS_AR_LOC Left Join " +
                                                              "(Select " +
                                                                   "ITEM_CCN.CCN, " +
                                                                   "ITEM_CCN.ITEM, " +
                                                                   "ITEM_CCN.REVISION, " +
                                                                   "ITEM_CCN.AGC, " +
                                                                   "ITEM.LENGTH, " +
                                                                   "ITEM.USER1, " +
                                                                   "ITEM.WEIGHT, " +
                                                                   "ITEM_CCN.USER2 As Die " +
                                                               "From " +
                                                                   "ITEM Inner Join " +
                                                                   "ITEM_CCN On ITEM_CCN.ITEM = ITEM.ITEM " +
                                                                           "And ITEM_CCN.REVISION = ITEM.REVISION) qryITEM_INFO " +
                                                                 "On qryITEM_INFO.CCN = SO.SALES_CCN " +
                                                                     "And qryITEM_INFO.ITEM = SO.ITEM " +
                                                                     "And qryITEM_INFO.REVISION = SO.REVISION Inner Join " +
                                                              "(Select " +
                                                                   "qryITEM_WEIGHTS.SALES_CCN, " +
                                                                   "qryITEM_WEIGHTS.MAS_LOC, " +
                                                                   "qryITEM_WEIGHTS.PACKLIST, " +
                                                                   "qryITEM_WEIGHTS.BOL, " +
                                                                   "Sum(qryITEM_WEIGHTS.TH_WGT) As STH_WGT " +
                                                               "From " +
                                                                    "(Select " +
                                                                         "SALES_CCN, " +
                                                                         "BOL, " +
                                                                         "MAS_LOC, " +
                                                                         "PACKLIST, " +
                                                                         "ITEM, " +
                                                                         "REVISION, " +
                                                                         "QTY, " +
                                                                         "WT, " +
                                                                         "WT_FT, " +
                                                                         "UM_SCALAR, " +
                                                                         "(Case " +
                                                                             "When SELL_UM = 'MT' " +
                                                                             "Then WT * UM_SCALAR * QTY " +
                                                                             "When SELL_UM = 'LB' " +
                                                                             "Then QTY " +
                                                                             "When SELL_UM = 'FT' " +
                                                                             "Then WT_FT * QTY " +
                                                                             "When SELL_UM = 'IN' " +
                                                                             "Then (WT_FT / 12) * QTY " +
                                                                             "Else WT * QTY " +
                                                                         "End) As TH_WGT " +
                                                                     "From " +
                                                                          "(Select " +
                                                                               "SO.SALES_CCN, " +
                                                                               "SO_SHIP.BOL, " +
                                                                               "SO_SHIP.MAS_LOC, " +
                                                                               "SO_SHIP.PACKLIST, " +
                                                                               "SO.ITEM, " +
                                                                               "SO.REVISION, " +
                                                                               "SO_SHIP.QTY, " +
                                                                               "SO.UM_SCALAR, " +
                                                                               "SO.SELL_UM, " +
                                                                               "(Case " +
                                                                                   "When ITEM.WEIGHT Is Null " +
                                                                                   "Then 0 " +
                                                                                   "Else ITEM.WEIGHT " +
                                                                               "End) As WT, " +
                                                                               "(Case " +
                                                                                   "When ITEM.USER1 Is Null " +
                                                                                   "Then '0' " +
                                                                                   "Else ITEM.USER1 " +
                                                                               "End) As WT_FT " +
                                                                           "From " +
                                                                               "SO Inner Join " +
                                                                               "SO_DEL On SO_DEL.SALES_CCN = SO.SALES_CCN " +
                                                                                       "And SO_DEL.SO = SO.SO " +
                                                                                       "And SO_DEL.SO_LINE = SO.SO_LINE Inner Join " +
                                                                               "SO_SHIP On SO_SHIP.SALES_CCN = SO_DEL.SALES_CCN " +
                                                                                       "And SO_SHIP.SO = SO_DEL.SO " +
                                                                                       "And SO_SHIP.SO_LINE = SO_DEL.SO_LINE " +
                                                                                       "And SO_SHIP.SO_DELIVERY = SO_DEL.SO_DELIVERY " +
                                                                               "Left Join " +
                                                                               "ITEM On ITEM.ITEM = SO.ITEM " +
                                                                                       "And ITEM.REVISION = SO.REVISION " +
                                                                           "Where " +
                                                                               "SO.SALES_CCN = 7)) qryITEM_WEIGHTS " +
                                                               "Group By " +
                                                                   "qryITEM_WEIGHTS.SALES_CCN, " +
                                                                   "qryITEM_WEIGHTS.MAS_LOC, " +
                                                                   "qryITEM_WEIGHTS.PACKLIST, " +
                                                                   "qryITEM_WEIGHTS.BOL) qryBOL_TOTALS On qryBOL_TOTALS.SALES_CCN = " +
                                                                     "BOL_HDR.CCN " +
                                                                     "And qryBOL_TOTALS.MAS_LOC = BOL_HDR.MAS_LOC " +
                                                                     "And qryBOL_TOTALS.PACKLIST = BOL_HDR.PACKLIST " +
                                                                     "And qryBOL_TOTALS.BOL = BOL_HDR.BOL)))) " +
                                 "Where " +
                                     "SALES_CCN = 7) qrySO_SHIP " +
                           "Where " +
                               "qrySO_SHIP.SALES_CCN = 7 " +
                           "Group By " +
                               "qrySO_SHIP.SALES_CCN, " +
                               "qrySO_SHIP.OTST_NAME, " +
                               "qrySO_SHIP.AR_DOC, " +
                               "qrySO_SHIP.CUSTOMER, " +
                               "qrySO_SHIP.USER_ALPHA1, " +
                               "qrySO_SHIP.Prim_Acct " +
                           "Order By " +
                               "qrySO_SHIP.OTST_NAME) qrySO_SHIP_GRP Inner Join " +
                          "(Select " +
                               "AR_DOC.AR_CCN, " +
                               "AR_DOC.AR_DOC_TYPE, " +
                               "AR_DOC.AR_DOC, " +
                               "AR_DOC.CCN, " +
                               "AR_DOC.AGC, " +
                               "AR_DOC.AMT, " +
                               "AR_DOC.CM_APPLIED_AMT, " +
                               "AR_DOC.BOOK_RATE, " +
                               "AR_DOC.CREATED_DATE, " +
                               "AR_DOC.CURRENCY, " +
                               "AR_DOC.CUS_PO, " +
                               "AR_DOC.DT_, " +
                               "AR_DOC.DISC_AMT, " +
                               "AR_DOC.DISC_APPLIED_AMT, " +
                               "AR_DOC.DISC_DUE_DATE, " +
                               "AR_DOC.DISC_EXCEPTION_AMT, " +
                               "AR_DOC.DISC_GRACE_DAYS, " +
                               "AR_DOC.DISC_PCT, " +
                               "AR_DOC.FROZEN_DATE, " +
                               "AR_DOC.NET_DUE_DATE, " +
                               "AR_DOC.PAID_AMT, " +
                               "AR_DOC.PAID_DATE, " +
                               "AR_DOC.ORIG_BOOK_RATE, " +
                               "AR_DOC.REASON, " +
                               "AR_DOC.REVAL_DATE, " +
                               "AR_DOC.SOURCE, " +
                               "AR_DOC.STMT_DATE, " +
                               "AR_DOC.TERMS_CODE, " +
                               "AR_DOC.VAT_AMT, " +
                               "AR_DOC.VAT_BASE_AMT, " +
                               "AR_DOC.VAT_REG, " +
                               "AR_DOC.WRITE_OFF_AMT, " +
                               "AR_DOC.WRITE_OFF_REASON, " +
                               "AR_DOC.NOTES_REC, " +
                               "AR_DOC.DISC_OVERRIDDEN, " +
                               "AR_DOC.VAT_EXEMPT, " +
                               "AR_DOC.SCHED_PMT, " +
                               "AR_DOC.LAST_VAR_SEQ, " +
                               "AR_DOC.LAST_PMT_SEQ, " +
                               "AR_DOC.BANK, " +
                               "AR_DOC.AR_DEP_SEQ, " +
                               "AR_DOC.AR_DEP_LINE, " +
                               "AR_DOC.UNAPPL_CASH_MEMO, " +
                               "AR_DOC.BOOK_RATE_DIV, " +
                               "AR_DOC.ORIG_BOOK_RATE_DIV, " +
                               "AR_DOC.VAT_EXEMPT_REASON, " +
                               "AR_DOC.CUSTOMER, " +
                               "AR_DOC.CUS_AR_LOC, " +
                               "AR_DOC.AR_DEP_DATE, " +
                               "AR_DOC.NET_DUE_ADATE, " +
                               "AR_DOC.WRITEOFF_APPROVER_NAME, " +
                               "AR_DOC.CONTRA_AMT, " +
                               "AR_DOC.CONTRA_NO, " +
                               "AR_DOC.AI, " +
                               "AR_DOC.DISPUTED, " +
                               "AR_DOC.REVERSE_DATE, " +
                               "AR_DOC.REVERSE_REASON, " +
                               "AR_DOC.REVERSE_MEMO, " +
                               "AR_DOC.PRINT_NOTES, " +
                               "AR_DOC.REVERSE_ASOF_DATE, " +
                               "AR_DOC.WRITE_OFF_ACCT, " +
                               "AR_DOC.TRANSACTION_TYPE_CODE, " +
                               "AR_DOC.VATPCD_CODE, " +
                               "AR_DOC.DISPATCH_VAT_REG, " +
                               "AR_DOC.LAST_ASOF_DATE, " +
                               "AR_DOC.LAST_TRANS_RATE, " +
                               "AR_DOC.LAST_TRANS_RATE_DATE, " +
                               "AR_DOC.LAST_TRANS_RATE_DIV, " +
                               "AR_DOC.LAST_TRANS_TYPE, " +
                               "AR_DOC.LAST_APPLIED_DATE, " +
                               "AR_DOC.USER_ALPHA1, " +
                               "AR_DOC.USER_ALPHA2, " +
                               "AR_DOC.USER_ALPHA3, " +
                               "AR_DOC.USER_NUM1, " +
                               "AR_DOC.USER_NUM2, " +
                               "AR_DOC.USER_NUM3, " +
                               "AR_DOC.USER_DATE, " +
                               "AR_DOC.USER_TIME, " +
                               "AR_DOC.IN_PAYMENT, " +
                               "AR_DOC.REFUND_AMT, " +
                               "AR_DOC.REFUND_DATE, " +
                               "AR_DOC.SEL_PMT_AMT, " +
                               "AR_DOC.AR_CRDET_SEQ, " +
                               "AR_DOC.BASE_DISC_AMT, " +
                               "AR_DOC.BASE_PAYMENT_AMT, " +
                               "AR_DOC.CHG_E7_AMT, " +
                               "AR_DOC.E7_AMT, " +
                               "AR_DOC.LAST_APPLIED_ACDET_SEQ, " +
                               "AR_DOC.LAST_APPLIED_DOC, " +
                               "AR_DOC.LAST_APPLIED_DOC_TYPE, " +
                               "AR_DOC.LAST_WOFF_SEQ " +
                           "From " +
                               "AR_DOC " +
                           "Where " +
                               "AR_DOC.CCN = 7 And " +
                               "AR_DOC.AR_DOC_TYPE = 'I' And " +
                               "AR_DOC.DT_ Between To_Date('" + begin + "', 'mm/dd/yy') And To_Date('" + end + "', 'mm/dd/yy')) qryAR_DOC On " +
                                 "qryAR_DOC.AR_CCN = qrySO_SHIP_GRP.SALES_CCN " +
                                 "And qryAR_DOC.AR_DOC = qrySO_SHIP_GRP.AR_DOC) qrySHIPPED_QTY_INV_AMT " +
                "Group By " +
                    "qrySHIPPED_QTY_INV_AMT.OTST_NAME, " +
                    "qrySHIPPED_QTY_INV_AMT.CUSTOMER " +
                "Order By " +
                    "qrySHIPPED_QTY_INV_AMT.OTST_NAME)", connection);

            OracleDataAdapter adapter = new OracleDataAdapter(cmd);

            connection.Open();
            adapter.Fill(ds, ds.Tables[6].TableName);
            adapter.Dispose();
            connection.Close();
            connection.Dispose();

            foreach (DataRow row in ds.SalesByCustomerPricePerLb)
            {
                row[6] = begin;
                row[7] = end;
            }

            ds.AcceptChanges();

            // ProcessingMode will be Either Remote or Local  
            rptViewer.ProcessingMode = ProcessingMode.Local;
            rptViewer.SizeToReportContent = true;
            rptViewer.ZoomMode = ZoomMode.FullPage;
            rptViewer.AsyncRendering = false;
            rptViewer.LocalReport.ReportPath = @"C:\Users\tbarrett\source\repos\ExpenseClient\ExpenseClient\Reports\SalesByCustomerPricePerLb.rdlc";
            ReportDataSource src = new ReportDataSource("SalesByCustomerPricePerLb", ds.Tables[6]); //@"C:\inetpub\wwwroot\Intranet\Reports\BookingsByAcctNum.rdlc";
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(src);

            ViewBag.ReportViewer = rptViewer;


            return View();
        }

        public ActionResult SalesByCustomer(SalesViewModel model)
        {
            ReportViewer rptViewer = new ReportViewer();
            G2ProdDataSet ds = new G2ProdDataSet();
            DateTime beginn = model.PromiseDateBegin.Date;
            DateTime endd = model.PromiseDateEnd.Date;
            string begin = beginn.Date.ToShortDateString();
            string end = endd.Date.ToShortDateString();
            int cxNum = model.CustomerNumber;

            string connectionString = "Data Source=(DESCRIPTION="
                    + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=gei-glovia)(PORT=1521)))"
                    + "(CONNECT_DATA=(SERVER=prod)(SERVICE_NAME=G2prod.gei.local)));"
                    + "User Id=glovia_prod;Password=manager;";

            OracleConnection connection = new OracleConnection(connectionString);

            OracleCommand cmd = new OracleCommand(
                "Select " +
                    "qrySHIPPED_QTY_INV_AMT.OTST_NAME, " +
                    "qrySHIPPED_QTY_INV_AMT.CUSTOMER, " +
                    "ROUND(Sum(qrySHIPPED_QTY_INV_AMT.SumSHIPPEDQTY), 2) As Sum_SHIPPEDQTY, " +
                    "ROUND(Sum(qrySHIPPED_QTY_INV_AMT.SumITEMWEIGHT), 2) As Sum_ITEMWEIGHT, " +
                    "ROUND(Sum(qrySHIPPED_QTY_INV_AMT.SumSALESAMT), 2) As Sum_SALESAMT " +
                "From " +
                    "(Select " +
                         "qrySO_SHIP_GRP.SALES_CCN, " +
                         "qrySO_SHIP_GRP.OTST_NAME, " +
                         "qrySO_SHIP_GRP.AR_DOC, " +
                         "qrySO_SHIP_GRP.SumSHIPPEDQTY, " +
                         "qrySO_SHIP_GRP.SumITEMWEIGHT, " +
                         "qrySO_SHIP_GRP.SumSALESAMT, " +
                         "qryAR_DOC.AMT, " +
                         "qrySO_SHIP_GRP.CUSTOMER, " +
                         "qrySO_SHIP_GRP.USER_ALPHA1, " +
                         "qrySO_SHIP_GRP.Prim_Acct " +
                     "From " +
                          "(Select " +
                               "qrySO_SHIP.SALES_CCN, " +
                               "qrySO_SHIP.OTST_NAME, " +
                               "qrySO_SHIP.AR_DOC, " +
                               "Sum(qrySO_SHIP.SHIPPED_QTY) As SumSHIPPEDQTY, " +
                               "Sum(qrySO_SHIP.ITEM_WEIGHT) As SumITEMWEIGHT, " +
                               "qrySO_SHIP.CUSTOMER, " +
                               "qrySO_SHIP.USER_ALPHA1, " +
                               "qrySO_SHIP.Prim_Acct, " +
                               "Sum(qrySO_SHIP.Sales_Amt) As SumSALESAMT " +
                           "From " +
                                "(Select " +
                                     "SALES_CCN, " +
                                     "SO, " +
                                     "SO_LINE, " +
                                     "AGC, " +
                                     "AGC_CODE, " +
                                     "ITEM, " +
                                     "REVISION, " +
                                     "SELL_UM, " +
                                     "SO_DELIVERY, " +
                                     "SO_SHIPMENT, " +
                                     "INVOICE_DATE, " +
                                     "SHIPPED_QTY, " +
                                     "NET_WEIGHT, " +
                                     "WT, " +
                                     "WT_FT, " +
                                     "TH_WGT, " +
                                     "MAS_LOC, " +
                                     "BOL, " +
                                     "STH_WGT, " +
                                     "TH_WT_PCT, " +
                                     "PCS_NET_WT, " +
                                     "ITEM_NET_WT, " +
                                     "(Case " +
                                         "When SubStr(AGC_CODE, 2) = 'OL' " +
                                         "Then 0 " +
                                         "When SELL_UM = 'MT' " +
                                         "Then ITEM_NET_WT * UM_SCALAR * SHIPPED_QTY " +
                                         "When SELL_UM = 'LB' " +
                                         "Then SHIPPED_QTY " +
                                         "When SELL_UM = 'FT' " +
                                         "Then WT_FT * SHIPPED_QTY " +
                                         "When SELL_UM = 'IN' " +
                                         "Then (WT_FT / 12) * SHIPPED_QTY " +
                                         "Else ITEM_NET_WT * SHIPPED_QTY " +
                                     "End) As ITEM_WEIGHT, " +
                                     "UM_SCALAR, " +
                                     "EXT_AMT As Sales_Amt, " +
                                     "AR_DOC, " +
                                     "CUSTOMER, " +
                                     "OTST_NAME, " +
                                     "USER_ALPHA1, " +
                                     "CUS_LOC, " +
                                     "(Case " +
                                         "When USER_ALPHA1 = ' ' " +
                                         "Then CUSTOMER " +
                                         "Else USER_ALPHA1 " +
                                     "End) As Prim_Acct " +
                                 "From " +
                                      "(Select " +
                                           "SALES_CCN, " +
                                           "SO, " +
                                           "SO_LINE, " +
                                           "AGC, " +
                                           "AGC_CODE, " +
                                           "ITEM, " +
                                           "REVISION, " +
                                           "Die, " +
                                           "SELL_UM, " +
                                           "SO_DELIVERY, " +
                                           "SO_SHIPMENT, " +
                                           "INVOICE_DATE, " +
                                           "SHIPPED_QTY, " +
                                           "NET_WEIGHT, " +
                                           "WT, " +
                                           "WT_FT, " +
                                           "TH_WGT, " +
                                           "BOL, " +
                                           "STH_WGT, " +
                                           "TH_WT_PCT, " +
                                           "PCS_NET_WT, " +
                                           "(Case " +
                                               "When SELL_UM = 'EA' " +
                                               "Then (PCS_NET_WT / SHIPPED_QTY) / UM_SCALAR " +
                                           "End) As ITEM_NET_WT, " +
                                           "UM_SCALAR, " +
                                           "EXT_AMT, " +
                                           "AR_DOC, " +
                                           "CUSTOMER, " +
                                           "OTST_NAME, " +
                                           "USER_ALPHA1, " +
                                           "MAS_LOC, " +
                                           "CUS_LOC " +
                                       "From " +
                                            "(Select " +
                                                 "SALES_CCN, " +
                                                 "SO, " +
                                                 "SO_LINE, " +
                                                 "AGC, " +
                                                 "AGC_CODE, " +
                                                 "ITEM, " +
                                                 "REVISION, " +
                                                 "Die, " +
                                                 "SELL_UM, " +
                                                 "SO_DELIVERY, " +
                                                 "SO_SHIPMENT, " +
                                                 "INVOICE_DATE, " +
                                                 "SHIPPED_QTY, " +
                                                 "NET_WEIGHT, " +
                                                 "WT, " +
                                                 "WT_FT, " +
                                                 "TH_WGT, " +
                                                 "BOL, " +
                                                 "STH_WGT, " +
                                                 "(Case " +
                                                     "When STH_WGT Is Null " +
                                                     "Then 0 " +
                                                     "Else (TH_WGT / STH_WGT) " +
                                                 "End) As TH_WT_PCT, " +
                                                 "(Case " +
                                                     "When STH_WGT Is Null " +
                                                     "Then 0 " +
                                                     "Else NET_WEIGHT * (TH_WGT / STH_WGT) " +
                                                 "End) As PCS_NET_WT, " +
                                                 "UM_SCALAR, " +
                                                 "EXT_AMT, " +
                                                 "AR_DOC, " +
                                                 "CUSTOMER, " +
                                                 "OTST_NAME, " +
                                                 "USER_ALPHA1, " +
                                                 "MAS_LOC, " +
                                                 "CUS_LOC " +
                                             "From " +
                                                  "(Select " +
                                                       "SALES_CCN, " +
                                                       "SO, " +
                                                       "SO_LINE, " +
                                                       "AGC, " +
                                                       "AGC_CODE, " +
                                                       "ITEM, " +
                                                       "REVISION, " +
                                                       "Die, " +
                                                       "SO_DELIVERY, " +
                                                       "SO_SHIPMENT, " +
                                                       "INVOICE_DATE, " +
                                                       "SHIPPED_QTY, " +
                                                       "NET_WEIGHT, " +
                                                       "WT, " +
                                                       "WT_FT, " +
                                                       "BOL, " +
                                                       "SELL_UM, " +
                                                       "UM_SCALAR, " +
                                                       "(Case " +
                                                           "When SELL_UM = 'MT' " +
                                                           "Then WT * UM_SCALAR * SHIPPED_QTY " +
                                                           "When SELL_UM = 'LB' " +
                                                           "Then SHIPPED_QTY " +
                                                           "When SELL_UM = 'FT' " +
                                                           "Then WT_FT * SHIPPED_QTY " +
                                                           "When SELL_UM = 'IN' " +
                                                           "Then (WT_FT / 12) * SHIPPED_QTY " +
                                                           "Else WT * SHIPPED_QTY " +
                                                       "End) As TH_WGT, " +
                                                       "STH_WGT, " +
                                                       "AR_DOC, " +
                                                       "EXT_AMT, " +
                                                       "CUSTOMER, " +
                                                       "OTST_NAME, " +
                                                       "USER_ALPHA1, " +
                                                       "MAS_LOC, " +
                                                       "CUS_LOC " +
                                                   "From " +
                                                        "(Select " +
                                                             "SO.SALES_CCN, " +
                                                             "SO.SO, " +
                                                             "SO.SO_LINE, " +
                                                             "SO.AGC, " +
                                                             "(Case " +
                                                                 "When qryITEM_INFO.AGC Is Null " +
                                                                 "Then SO.AGC " +
                                                                 "Else qryITEM_INFO.AGC " +
                                                             "End) As AGC_CODE, " +
                                                             "SO.ITEM, " +
                                                             "SO.REVISION, " +
                                                             "qryITEM_INFO.Die, " +
                                                             "SO.SELL_UM, " +
                                                             "SO_DEL.SO_DELIVERY, " +
                                                             "qrySO_SHIP_DTRNG.SO_SHIPMENT, " +
                                                             "qrySO_SHIP_DTRNG.INVOICE_DATE, " +
                                                             "qrySO_SHIP_DTRNG.QTY As SHIPPED_QTY, " +
                                                             "BOL_HDR.NET_WEIGHT, " +
                                                             "(Case " +
                                                                 "When qryITEM_INFO.WEIGHT Is Null " +
                                                                 "Then 0 " +
                                                                 "Else qryITEM_INFO.WEIGHT " +
                                                             "End) As WT, " +
                                                             "(Case " +
                                                                 "When qryITEM_INFO.USER1 Is Null " +
                                                                 "Then '0' " +
                                                                 "Else qryITEM_INFO.USER1 " +
                                                             "End) As WT_FT, " +
                                                             "BOL_HDR.BOL, " +
                                                             "qryBOL_TOTALS.STH_WGT, " +
                                                             "SO.UM_SCALAR, " +
                                                             "SO.UNIT_PRICE, " +
                                                             "qrySO_SHIP_DTRNG.AR_DOC, " +
                                                             "qrySO_SHIP_DTRNG.EXT_AMT, " +
                                                             "SO_HDR.CUSTOMER, " +
                                                             "CUS_LOC.NAME As OTST_NAME, " +
                                                             "CUS_LOC.USER_ALPHA1, " +
                                                             "BOL_HDR.MAS_LOC, " +
                                                             "CUS_LOC.CUS_LOC " +
                                                         "From " +
                                                             "BOL_HDR Inner Join " +
                                                              "(Select " +
                                                                   "* " +
                                                               "From " +
                                                                   "SO_SHIP " +
                                                               "Where " +
                                                                   "SO_SHIP.SALES_CCN = 7 And " +
                                                                   "SO_SHIP.INVOICE_DATE Between To_Date('" + begin + "', 'mm/dd/yy') And " +
                                                                   "To_Date('" + end + "', 'mm/dd/yy')) qrySO_SHIP_DTRNG On " +
                                                                     "BOL_HDR.CCN = qrySO_SHIP_DTRNG.SALES_CCN " +
                                                                     "And BOL_HDR.MAS_LOC = qrySO_SHIP_DTRNG.MAS_LOC " +
                                                                     "And BOL_HDR.PACKLIST = qrySO_SHIP_DTRNG.PACKLIST Inner Join " +
                                                             "SO_DEL On qrySO_SHIP_DTRNG.SALES_CCN = SO_DEL.SALES_CCN " +
                                                                     "And qrySO_SHIP_DTRNG.SO = SO_DEL.SO " +
                                                                     "And qrySO_SHIP_DTRNG.SO_LINE = SO_DEL.SO_LINE " +
                                                                     "And qrySO_SHIP_DTRNG.SO_DELIVERY = SO_DEL.SO_DELIVERY Inner Join " +
                                                             "SO On SO_DEL.SALES_CCN = SO.SALES_CCN " +
                                                                     "And SO_DEL.SO = SO.SO " +
                                                                     "And SO_DEL.SO_LINE = SO.SO_LINE Inner Join " +
                                                             "SO_HDR On SO_HDR.SALES_CCN = SO.SALES_CCN " +
                                                                     "And SO_HDR.SO = SO.SO Left Join " +
                                                             "CUS_LOC On CUS_LOC.CUSTOMER = SO_HDR.CUSTOMER " +
                                                                     "And CUS_LOC.CUS_LOC = SO_HDR.CUS_AR_LOC Left Join " +
                                                              "(Select " +
                                                                   "ITEM_CCN.CCN, " +
                                                                   "ITEM_CCN.ITEM, " +
                                                                   "ITEM_CCN.REVISION, " +
                                                                   "ITEM_CCN.AGC, " +
                                                                   "ITEM.LENGTH, " +
                                                                   "ITEM.USER1, " +
                                                                   "ITEM.WEIGHT, " +
                                                                   "ITEM_CCN.USER2 As Die " +
                                                               "From " +
                                                                   "ITEM Inner Join " +
                                                                   "ITEM_CCN On ITEM_CCN.ITEM = ITEM.ITEM " +
                                                                           "And ITEM_CCN.REVISION = ITEM.REVISION) qryITEM_INFO " +
                                                                 "On qryITEM_INFO.CCN = SO.SALES_CCN " +
                                                                     "And qryITEM_INFO.ITEM = SO.ITEM " +
                                                                     "And qryITEM_INFO.REVISION = SO.REVISION Inner Join " +
                                                              "(Select " +
                                                                   "qryITEM_WEIGHTS.SALES_CCN, " +
                                                                   "qryITEM_WEIGHTS.MAS_LOC, " +
                                                                   "qryITEM_WEIGHTS.PACKLIST, " +
                                                                   "qryITEM_WEIGHTS.BOL, " +
                                                                   "Sum(qryITEM_WEIGHTS.TH_WGT) As STH_WGT " +
                                                               "From " +
                                                                    "(Select " +
                                                                         "SALES_CCN, " +
                                                                         "BOL, " +
                                                                         "MAS_LOC, " +
                                                                         "PACKLIST, " +
                                                                         "ITEM, " +
                                                                         "REVISION, " +
                                                                         "QTY, " +
                                                                         "WT, " +
                                                                         "WT_FT, " +
                                                                         "UM_SCALAR, " +
                                                                         "(Case " +
                                                                             "When SELL_UM = 'MT' " +
                                                                             "Then WT * UM_SCALAR * QTY " +
                                                                             "When SELL_UM = 'LB' " +
                                                                             "Then QTY " +
                                                                             "When SELL_UM = 'FT' " +
                                                                             "Then WT_FT * QTY " +
                                                                             "When SELL_UM = 'IN' " +
                                                                             "Then (WT_FT / 12) * QTY " +
                                                                             "Else WT * QTY " +
                                                                         "End) As TH_WGT " +
                                                                     "From " +
                                                                          "(Select " +
                                                                               "SO.SALES_CCN, " +
                                                                               "SO_SHIP.BOL, " +
                                                                               "SO_SHIP.MAS_LOC, " +
                                                                               "SO_SHIP.PACKLIST, " +
                                                                               "SO.ITEM, " +
                                                                               "SO.REVISION, " +
                                                                               "SO_SHIP.QTY, " +
                                                                               "SO.UM_SCALAR, " +
                                                                               "SO.SELL_UM, " +
                                                                               "(Case " +
                                                                                   "When ITEM.WEIGHT Is Null " +
                                                                                   "Then 0 " +
                                                                                   "Else ITEM.WEIGHT " +
                                                                               "End) As WT, " +
                                                                               "(Case " +
                                                                                   "When ITEM.USER1 Is Null " +
                                                                                   "Then '0' " +
                                                                                   "Else ITEM.USER1 " +
                                                                               "End) As WT_FT " +
                                                                           "From " +
                                                                               "SO Inner Join " +
                                                                               "SO_DEL On SO_DEL.SALES_CCN = SO.SALES_CCN " +
                                                                                       "And SO_DEL.SO = SO.SO " +
                                                                                       "And SO_DEL.SO_LINE = SO.SO_LINE Inner Join " +
                                                                               "SO_SHIP On SO_SHIP.SALES_CCN = SO_DEL.SALES_CCN " +
                                                                                       "And SO_SHIP.SO = SO_DEL.SO " +
                                                                                       "And SO_SHIP.SO_LINE = SO_DEL.SO_LINE " +
                                                                                       "And SO_SHIP.SO_DELIVERY = SO_DEL.SO_DELIVERY " +
                                                                               "Left Join " +
                                                                               "ITEM On ITEM.ITEM = SO.ITEM " +
                                                                                       "And ITEM.REVISION = SO.REVISION " +
                                                                           "Where " +
                                                                               "SO.SALES_CCN = 7)) qryITEM_WEIGHTS " +
                                                               "Group By " +
                                                                   "qryITEM_WEIGHTS.SALES_CCN, " +
                                                                   "qryITEM_WEIGHTS.MAS_LOC, " +
                                                                   "qryITEM_WEIGHTS.PACKLIST, " +
                                                                   "qryITEM_WEIGHTS.BOL) qryBOL_TOTALS On qryBOL_TOTALS.SALES_CCN = " +
                                                                     "BOL_HDR.CCN " +
                                                                     "And qryBOL_TOTALS.MAS_LOC = BOL_HDR.MAS_LOC " +
                                                                     "And qryBOL_TOTALS.PACKLIST = BOL_HDR.PACKLIST " +
                                                                     "And qryBOL_TOTALS.BOL = BOL_HDR.BOL)))) " +
                                 "Where " +
                                     "SALES_CCN = 7) qrySO_SHIP " +
                           "Where " +
                               "qrySO_SHIP.SALES_CCN = 7 " +
                           "Group By " +
                               "qrySO_SHIP.SALES_CCN, " +
                               "qrySO_SHIP.OTST_NAME, " +
                               "qrySO_SHIP.AR_DOC, " +
                               "qrySO_SHIP.CUSTOMER, " +
                               "qrySO_SHIP.USER_ALPHA1, " +
                               "qrySO_SHIP.Prim_Acct " +
                           "Order By " +
                               "qrySO_SHIP.OTST_NAME) qrySO_SHIP_GRP Inner Join " +
                          "(Select " +
                               "AR_DOC.AR_CCN, " +
                               "AR_DOC.AR_DOC_TYPE, " +
                               "AR_DOC.AR_DOC, " +
                               "AR_DOC.CCN, " +
                               "AR_DOC.AGC, " +
                               "AR_DOC.AMT, " +
                               "AR_DOC.CM_APPLIED_AMT, " +
                               "AR_DOC.BOOK_RATE, " +
                               "AR_DOC.CREATED_DATE, " +
                               "AR_DOC.CURRENCY, " +
                               "AR_DOC.CUS_PO, " +
                               "AR_DOC.DT_, " +
                               "AR_DOC.DISC_AMT, " +
                               "AR_DOC.DISC_APPLIED_AMT, " +
                               "AR_DOC.DISC_DUE_DATE, " +
                               "AR_DOC.DISC_EXCEPTION_AMT, " +
                               "AR_DOC.DISC_GRACE_DAYS, " +
                               "AR_DOC.DISC_PCT, " +
                               "AR_DOC.FROZEN_DATE, " +
                               "AR_DOC.NET_DUE_DATE, " +
                               "AR_DOC.PAID_AMT, " +
                               "AR_DOC.PAID_DATE, " +
                               "AR_DOC.ORIG_BOOK_RATE, " +
                               "AR_DOC.REASON, " +
                               "AR_DOC.REVAL_DATE, " +
                               "AR_DOC.SOURCE, " +
                               "AR_DOC.STMT_DATE, " +
                               "AR_DOC.TERMS_CODE, " +
                               "AR_DOC.VAT_AMT, " +
                               "AR_DOC.VAT_BASE_AMT, " +
                               "AR_DOC.VAT_REG, " +
                               "AR_DOC.WRITE_OFF_AMT, " +
                               "AR_DOC.WRITE_OFF_REASON, " +
                               "AR_DOC.NOTES_REC, " +
                               "AR_DOC.DISC_OVERRIDDEN, " +
                               "AR_DOC.VAT_EXEMPT, " +
                               "AR_DOC.SCHED_PMT, " +
                               "AR_DOC.LAST_VAR_SEQ, " +
                               "AR_DOC.LAST_PMT_SEQ, " +
                               "AR_DOC.BANK, " +
                               "AR_DOC.AR_DEP_SEQ, " +
                               "AR_DOC.AR_DEP_LINE, " +
                               "AR_DOC.UNAPPL_CASH_MEMO, " +
                               "AR_DOC.BOOK_RATE_DIV, " +
                               "AR_DOC.ORIG_BOOK_RATE_DIV, " +
                               "AR_DOC.VAT_EXEMPT_REASON, " +
                               "AR_DOC.CUSTOMER, " +
                               "AR_DOC.CUS_AR_LOC, " +
                               "AR_DOC.AR_DEP_DATE, " +
                               "AR_DOC.NET_DUE_ADATE, " +
                               "AR_DOC.WRITEOFF_APPROVER_NAME, " +
                               "AR_DOC.CONTRA_AMT, " +
                               "AR_DOC.CONTRA_NO, " +
                               "AR_DOC.AI, " +
                               "AR_DOC.DISPUTED, " +
                               "AR_DOC.REVERSE_DATE, " +
                               "AR_DOC.REVERSE_REASON, " +
                               "AR_DOC.REVERSE_MEMO, " +
                               "AR_DOC.PRINT_NOTES, " +
                               "AR_DOC.REVERSE_ASOF_DATE, " +
                               "AR_DOC.WRITE_OFF_ACCT, " +
                               "AR_DOC.TRANSACTION_TYPE_CODE, " +
                               "AR_DOC.VATPCD_CODE, " +
                               "AR_DOC.DISPATCH_VAT_REG, " +
                               "AR_DOC.LAST_ASOF_DATE, " +
                               "AR_DOC.LAST_TRANS_RATE, " +
                               "AR_DOC.LAST_TRANS_RATE_DATE, " +
                               "AR_DOC.LAST_TRANS_RATE_DIV, " +
                               "AR_DOC.LAST_TRANS_TYPE, " +
                               "AR_DOC.LAST_APPLIED_DATE, " +
                               "AR_DOC.USER_ALPHA1, " +
                               "AR_DOC.USER_ALPHA2, " +
                               "AR_DOC.USER_ALPHA3, " +
                               "AR_DOC.USER_NUM1, " +
                               "AR_DOC.USER_NUM2, " +
                               "AR_DOC.USER_NUM3, " +
                               "AR_DOC.USER_DATE, " +
                               "AR_DOC.USER_TIME, " +
                               "AR_DOC.IN_PAYMENT, " +
                               "AR_DOC.REFUND_AMT, " +
                               "AR_DOC.REFUND_DATE, " +
                               "AR_DOC.SEL_PMT_AMT, " +
                               "AR_DOC.AR_CRDET_SEQ, " +
                               "AR_DOC.BASE_DISC_AMT, " +
                               "AR_DOC.BASE_PAYMENT_AMT, " +
                               "AR_DOC.CHG_E7_AMT, " +
                               "AR_DOC.E7_AMT, " +
                               "AR_DOC.LAST_APPLIED_ACDET_SEQ, " +
                               "AR_DOC.LAST_APPLIED_DOC, " +
                               "AR_DOC.LAST_APPLIED_DOC_TYPE, " +
                               "AR_DOC.LAST_WOFF_SEQ " +
                           "From " +
                               "AR_DOC " +
                           "Where " +
                               "AR_DOC.CCN = 7 And " +
                               "AR_DOC.AR_DOC_TYPE = 'I' And " +
                               "AR_DOC.DT_ Between To_Date('" + begin + "', 'mm/dd/yy') And To_Date('" + end + "', 'mm/dd/yy')) qryAR_DOC On " +
                                 "qryAR_DOC.AR_CCN = qrySO_SHIP_GRP.SALES_CCN " +
                                 "And qryAR_DOC.AR_DOC = qrySO_SHIP_GRP.AR_DOC) qrySHIPPED_QTY_INV_AMT " +
                "Group By " +
                    "qrySHIPPED_QTY_INV_AMT.OTST_NAME, " +
                    "qrySHIPPED_QTY_INV_AMT.CUSTOMER " +
                "Order By " +
                    "qrySHIPPED_QTY_INV_AMT.OTST_NAME", connection);

            OracleDataAdapter adapter = new OracleDataAdapter(cmd);

            connection.Open();
            adapter.Fill(ds, ds.Tables[5].TableName);
            adapter.Dispose();
            connection.Close();
            connection.Dispose();

            foreach (DataRow row in ds.SalesByCustomer)
            {
                row[5] = begin;
                row[6] = end;
            }

            ds.AcceptChanges();

            // ProcessingMode will be Either Remote or Local  
            rptViewer.ProcessingMode = ProcessingMode.Local;
            rptViewer.SizeToReportContent = true;
            rptViewer.ZoomMode = ZoomMode.FullPage;
            rptViewer.AsyncRendering = false;
            rptViewer.LocalReport.ReportPath = @"C:\Users\tbarrett\source\repos\ExpenseClient\ExpenseClient\Reports\SalesByCustomer.rdlc";
            ReportDataSource src = new ReportDataSource("SalesByCustomer", ds.Tables[5]); //@"C:\inetpub\wwwroot\Intranet\Reports\BookingsByAcctNum.rdlc";
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(src);

            ViewBag.ReportViewer = rptViewer;

            return View();
        }

        public ActionResult SalesByItemByAcctNum(SalesViewModel model)
        {
            ReportViewer rptViewer = new ReportViewer();
            G2ProdDataSet ds = new G2ProdDataSet();
            DateTime beginn = model.PromiseDateBegin.Date;
            DateTime endd = model.PromiseDateEnd.Date;
            string begin = beginn.Date.ToShortDateString();
            string end = endd.Date.ToShortDateString();
            int cxNum = model.CustomerNumber;

            string connectionString = "Data Source=(DESCRIPTION="
                    + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=gei-glovia)(PORT=1521)))"
                    + "(CONNECT_DATA=(SERVER=prod)(SERVICE_NAME=G2prod.gei.local)));"
                    + "User Id=glovia_prod;Password=manager;";

            OracleConnection connection = new OracleConnection(connectionString);

            OracleCommand cmd = new OracleCommand(
                "Select " +
                    "qrySO_SHIP_BYACCT.SALES_CCN, " +
                    "qrySO_SHIP_BYACCT.OTST_NAME, " +
                    "qrySO_SHIP_BYACCT.CUSTOMER, " +
                    "qrySO_SHIP_BYACCT.ITEM, " +
                    "qrySO_SHIP_BYACCT.REVISION, " +
                    "ROUND(Sum(qrySO_SHIP_BYACCT.EXT_AMT), 2) As Sales_Amt, " +
                    "ROUND(Sum(qrySO_SHIP_BYACCT.SHIPPED_QTY), 2) As SumSHIPPEDQTY, " +
                    "ROUND(Sum(qrySO_SHIP_BYACCT.ITEM_WEIGHT), 2) As SumITEMWEIGHT " +
                "From " +
                    "(Select " +
                         "SALES_CCN, SO, SO_LINE, AGC, AGC_CODE, ITEM, REVISION, Die, SELL_UM, SO_DELIVERY, SO_SHIPMENT, " +
                         "INVOICE_DATE, SHIPPED_QTY, NET_WEIGHT, " +
                         "WT, WT_FT, TH_WGT, BOL, STH_WGT, TH_WT_PCT, PCS_NET_WT, ITEM_NET_WT, " +
                         "(Case " +
                             "When SubStr(AGC_CODE, 2) = 'OL' " +
                             "Then 0 " +
                             "When SELL_UM = 'MT' " +
                             "Then ITEM_NET_WT * UM_SCALAR * SHIPPED_QTY " +
                             "When SELL_UM = 'LB' " +
                             "Then SHIPPED_QTY " +
                             "When SELL_UM = 'FT' " +
                             "Then WT_FT * SHIPPED_QTY " +
                             "When SELL_UM = 'IN' " +
                             "Then (WT_FT / 12) * SHIPPED_QTY " +
                             "Else ITEM_NET_WT * SHIPPED_QTY " +
                         "End) As ITEM_WEIGHT, " +
                         "UM_SCALAR, EXT_AMT, AR_DOC, CUSTOMER, OTST_NAME " +
                     "From " +
                          "(Select " +
                               "SALES_CCN, SO, SO_LINE, AGC, AGC_CODE, ITEM, REVISION, Die, SELL_UM, SO_DELIVERY, SO_SHIPMENT, " +
                               "INVOICE_DATE, SHIPPED_QTY, " +
                               "NET_WEIGHT, WT, WT_FT, TH_WGT, BOL, " +
                               "STH_WGT, " +
                               "TH_WT_PCT, " +
                               "PCS_NET_WT, " +
                               "(Case " +
                                   "When SELL_UM = 'EA' " +
                                   "Then (PCS_NET_WT / SHIPPED_QTY) / UM_SCALAR " +
                               "End) As ITEM_NET_WT, " +
                               "UM_SCALAR, " +
                               "EXT_AMT, " +
                               "AR_DOC, " +
                               "CUSTOMER, " +
                               "OTST_NAME " +
                           "From " +
                                "(Select " +
                                     "SALES_CCN, " +
                                     "SO, " +
                                     "SO_LINE, " +
                                     "AGC, " +
                                     "AGC_CODE, " +
                                     "ITEM, " +
                                     "REVISION, " +
                                     "Die, " +
                                     "SELL_UM, " +
                                     "SO_DELIVERY, " +
                                     "SO_SHIPMENT, " +
                                     "INVOICE_DATE, " +
                                     "SHIPPED_QTY, " +
                                     "NET_WEIGHT, " +
                                     "WT, " +
                                     "WT_FT, " +
                                     "TH_WGT, " +
                                     "BOL, " +
                                     "STH_WGT, " +
                                     "(Case " +
                                         "When STH_WGT Is Null " +
                                         "Then 0 " +
                                         "Else (TH_WGT / STH_WGT) " +
                                     "End) As TH_WT_PCT, " +
                                     "(Case " +
                                         "When STH_WGT Is Null " +
                                         "Then 0 " +
                                         "Else NET_WEIGHT * (TH_WGT / STH_WGT) " +
                                     "End) As PCS_NET_WT, " +
                                     "UM_SCALAR, " +
                                     "EXT_AMT, " +
                                     "AR_DOC, " +
                                     "CUSTOMER, " +
                                     "OTST_NAME " +
                                 "From " +
                                      "(Select " +
                                           "SALES_CCN, " +
                                           "SO, " +
                                           "SO_LINE, " +
                                           "AGC, " +
                                           "AGC_CODE, " +
                                           "ITEM, " +
                                           "REVISION, " +
                                           "Die, " +
                                           "SO_DELIVERY, " +
                                           "SO_SHIPMENT, " +
                                           "INVOICE_DATE, " +
                                           "SHIPPED_QTY, " +
                                           "NET_WEIGHT, " +
                                           "WT, " +
                                           "WT_FT, " +
                                           "BOL, " +
                                           "SELL_UM, " +
                                           "UM_SCALAR, " +
                                           "(Case " +
                                               "When SELL_UM = 'MT' " +
                                               "Then WT * UM_SCALAR * SHIPPED_QTY " +
                                               "When SELL_UM = 'LB' " +
                                               "Then SHIPPED_QTY " +
                                               "When SELL_UM = 'FT' " +
                                               "Then WT_FT * SHIPPED_QTY " +
                                               "When SELL_UM = 'IN' " +
                                               "Then (WT_FT / 12) * SHIPPED_QTY " +
                                               "Else WT * SHIPPED_QTY " +
                                           "End) As TH_WGT, " +
                                           "STH_WGT, " +
                                           "AR_DOC, " +
                                           "EXT_AMT, " +
                                           "CUSTOMER, " +
                                           "OTST_NAME " +
                                       "From " +
                                            "(Select " +
                                                 "SO.SALES_CCN, " +
                                                 "SO.SO, " +
                                                 "SO.SO_LINE, " +
                                                 "SO.AGC, " +
                                                 "(Case " +
                                                     "When qryITEM_INFO.AGC Is Null " +
                                                     "Then SO.AGC " +
                                                     "Else qryITEM_INFO.AGC " +
                                                 "End) As AGC_CODE, " +
                                                 "SO.ITEM, " +
                                                 "SO.REVISION, " +
                                                 "qryITEM_INFO.Die, " +
                                                 "SO.SELL_UM, " +
                                                 "SO_DEL.SO_DELIVERY, " +
                                                 "qrySO_SHIP_DTRNG.SO_SHIPMENT, " +
                                                 "qrySO_SHIP_DTRNG.INVOICE_DATE, " +
                                                 "qrySO_SHIP_DTRNG.QTY As SHIPPED_QTY, " +
                                                 "BOL_HDR.NET_WEIGHT, " +
                                                 "(Case " +
                                                     "When qryITEM_INFO.WEIGHT Is Null " +
                                                     "Then 0 " +
                                                     "Else qryITEM_INFO.WEIGHT " +
                                                 "End) As WT, " +
                                                 "(Case " +
                                                     "When qryITEM_INFO.USER1 Is Null " +
                                                     "Then '0' " +
                                                     "Else qryITEM_INFO.USER1 " +
                                                 "End) As WT_FT, " +
                                                 "BOL_HDR.BOL, " +
                                                 "qryBOL_TOTALS.STH_WGT, " +
                                                 "SO.UM_SCALAR, " +
                                                 "SO.UNIT_PRICE, " +
                                                 "qrySO_SHIP_DTRNG.AR_DOC, " +
                                                 "qrySO_SHIP_DTRNG.EXT_AMT, " +
                                                 "SO_HDR.CUSTOMER, " +
                                                 "CUS_LOC.NAME As OTST_NAME " +
                                             "From " +
                                                 "BOL_HDR Inner Join " +
                                                  "(Select " +
                                                       "* " +
                                                   "From " +
                                                       "SO_SHIP " +
                                                   "Where " +
                                                       "SO_SHIP.SALES_CCN = 7 And " +
                                                       "SO_SHIP.INVOICE_DATE Between To_Date('" + begin + "', 'mm/dd/yy') And " +
                                                       "To_Date('" + end + "', 'mm/dd/yy')) qrySO_SHIP_DTRNG On " +
                                                         "BOL_HDR.CCN = qrySO_SHIP_DTRNG.SALES_CCN " +
                                                         "And BOL_HDR.MAS_LOC = qrySO_SHIP_DTRNG.MAS_LOC " +
                                                         "And BOL_HDR.PACKLIST = qrySO_SHIP_DTRNG.PACKLIST Inner Join " +
                                                 "SO_DEL On qrySO_SHIP_DTRNG.SALES_CCN = SO_DEL.SALES_CCN " +
                                                         "And qrySO_SHIP_DTRNG.SO = SO_DEL.SO " +
                                                         "And qrySO_SHIP_DTRNG.SO_LINE = SO_DEL.SO_LINE " +
                                                         "And qrySO_SHIP_DTRNG.SO_DELIVERY = SO_DEL.SO_DELIVERY Inner Join " +
                                                 "SO On SO_DEL.SALES_CCN = SO.SALES_CCN " +
                                                         "And SO_DEL.SO = SO.SO " +
                                                         "And SO_DEL.SO_LINE = SO.SO_LINE Inner Join " +
                                                 "SO_HDR On SO_HDR.SALES_CCN = SO.SALES_CCN " +
                                                         "And SO_HDR.SO = SO.SO Left Join " +
                                                 "CUS_LOC On CUS_LOC.CUSTOMER = SO_HDR.CUSTOMER " +
                                                         "And CUS_LOC.CUS_LOC = SO_HDR.CUS_AR_LOC Left Join " +
                                                  "(Select " +
                                                       "ITEM_CCN.CCN, " +
                                                       "ITEM_CCN.ITEM, " +
                                                       "ITEM_CCN.REVISION, " +
                                                       "ITEM_CCN.AGC, " +
                                                       "ITEM.LENGTH, " +
                                                       "ITEM.USER1, " +
                                                       "ITEM.WEIGHT, " +
                                                       "ITEM_CCN.USER2 As Die " +
                                                   "From " +
                                                       "ITEM Inner Join " +
                                                       "ITEM_CCN On ITEM_CCN.ITEM = ITEM.ITEM " +
                                                               "And ITEM_CCN.REVISION = ITEM.REVISION) qryITEM_INFO On " +
                                                         "qryITEM_INFO.CCN = SO.SALES_CCN " +
                                                         "And qryITEM_INFO.ITEM = SO.ITEM " +
                                                         "And qryITEM_INFO.REVISION = SO.REVISION Inner Join " +
                                                  "(Select " +
                                                       "qryITEM_WEIGHTS.SALES_CCN, " +
                                                       "qryITEM_WEIGHTS.MAS_LOC, " +
                                                       "qryITEM_WEIGHTS.PACKLIST, " +
                                                       "qryITEM_WEIGHTS.BOL, " +
                                                       "Sum(qryITEM_WEIGHTS.TH_WGT) As STH_WGT " +
                                                   "From " +
                                                        "(Select " +
                                                             "SALES_CCN, " +
                                                             "BOL, " +
                                                             "MAS_LOC, " +
                                                             "PACKLIST, " +
                                                             "ITEM, " +
                                                             "REVISION, " +
                                                             "QTY, " +
                                                             "WT, " +
                                                             "WT_FT, " +
                                                             "UM_SCALAR, " +
                                                             "(Case " +
                                                                 "When SELL_UM = 'MT' " +
                                                                 "Then WT * UM_SCALAR * QTY " +
                                                                 "When SELL_UM = 'LB' " +
                                                                 "Then QTY " +
                                                                 "When SELL_UM = 'FT' " +
                                                                 "Then WT_FT * QTY " +
                                                                 "When SELL_UM = 'IN' " +
                                                                 "Then (WT_FT / 12) * QTY " +
                                                                 "Else WT * QTY " +
                                                             "End) As TH_WGT " +
                                                         "From " +
                                                              "(Select " +
                                                                   "SO.SALES_CCN, " +
                                                                   "SO_SHIP.BOL, " +
                                                                   "SO_SHIP.MAS_LOC, " +
                                                                   "SO_SHIP.PACKLIST, " +
                                                                   "SO.ITEM, " +
                                                                   "SO.REVISION, " +
                                                                   "SO_SHIP.QTY, " +
                                                                   "SO.UM_SCALAR, " +
                                                                   "SO.SELL_UM, " +
                                                                   "(Case " +
                                                                       "When ITEM.WEIGHT Is Null " +
                                                                       "Then 0 " +
                                                                       "Else ITEM.WEIGHT " +
                                                                   "End) As WT, " +
                                                                   "(Case " +
                                                                       "When ITEM.USER1 Is Null " +
                                                                       "Then '0' " +
                                                                       "Else ITEM.USER1 " +
                                                                   "End) As WT_FT " +
                                                               "From " +
                                                                   "SO Inner Join " +
                                                                   "SO_DEL On SO_DEL.SALES_CCN = SO.SALES_CCN " +
                                                                           "And SO_DEL.SO = SO.SO " +
                                                                           "And SO_DEL.SO_LINE = SO.SO_LINE Inner Join " +
                                                                   "SO_SHIP On SO_SHIP.SALES_CCN = SO_DEL.SALES_CCN " +
                                                                           "And SO_SHIP.SO = SO_DEL.SO " +
                                                                           "And SO_SHIP.SO_LINE = SO_DEL.SO_LINE " +
                                                                           "And SO_SHIP.SO_DELIVERY = SO_DEL.SO_DELIVERY Left Join " +
                                                                   "ITEM On ITEM.ITEM = SO.ITEM " +
                                                                           "And ITEM.REVISION = SO.REVISION " +
                                                               "Where " +
                                                                   "SO.SALES_CCN = 7)) qryITEM_WEIGHTS " +
                                                   "Group By " +
                                                       "qryITEM_WEIGHTS.SALES_CCN, " +
                                                       "qryITEM_WEIGHTS.MAS_LOC, " +
                                                       "qryITEM_WEIGHTS.PACKLIST, " +
                                                       "qryITEM_WEIGHTS.BOL) qryBOL_TOTALS On qryBOL_TOTALS.SALES_CCN = BOL_HDR.CCN " +
                                                         "And qryBOL_TOTALS.MAS_LOC = BOL_HDR.MAS_LOC " +
                                                         "And qryBOL_TOTALS.PACKLIST = BOL_HDR.PACKLIST " +
                                                         "And qryBOL_TOTALS.BOL = BOL_HDR.BOL " +
                                             "Where " +
                                                 "SO_HDR.CUSTOMER = '" + cxNum + "'))))) qrySO_SHIP_BYACCT " +
                                                 "Group By SALES_CCN, OTST_NAME, CUSTOMER, ITEM, REVISION", connection);


            OracleDataAdapter adapter = new OracleDataAdapter(cmd);

            connection.Open();
            adapter.Fill(ds, ds.Tables[4].TableName);
            adapter.Dispose();
            connection.Close();
            connection.Dispose();

            foreach (DataRow row in ds.SalesByItemByAcctNum)
            {
                row[8] = begin;
                row[9] = end;
            }

            ds.AcceptChanges();

            // ProcessingMode will be Either Remote or Local  
            rptViewer.ProcessingMode = ProcessingMode.Local;
            rptViewer.SizeToReportContent = true;
            rptViewer.ZoomMode = ZoomMode.FullPage;
            rptViewer.AsyncRendering = false;
            rptViewer.LocalReport.ReportPath = @"C:\Users\tbarrett\source\repos\ExpenseClient\ExpenseClient\Reports\SalesByItemByAcctNum.rdlc";
            ReportDataSource src = new ReportDataSource("SalesByItemByAcctNum", ds.Tables[4]); //@"C:\inetpub\wwwroot\Intranet\Reports\BookingsByAcctNum.rdlc";
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(src);

            ViewBag.ReportViewer = rptViewer;
            return View();
        }

        public ActionResult SalesByDieByAcctNum(SalesViewModel model)
        {
            ReportViewer rptViewer = new ReportViewer();
            G2ProdDataSet ds = new G2ProdDataSet();
            DateTime beginn = model.PromiseDateBegin.Date.Date;
            DateTime endd = model.PromiseDateEnd.Date.Date;
            int cxNum = model.CustomerNumber;
            string begin = beginn.Date.ToShortDateString();
            string end = endd.Date.ToShortDateString();

            string connectionString = "Data Source=(DESCRIPTION="
                    + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=gei-glovia)(PORT=1521)))"
                    + "(CONNECT_DATA=(SERVER=prod)(SERVICE_NAME=G2prod.gei.local)));"
                    + "User Id=glovia_prod;Password=manager;";

            OracleConnection connection = new OracleConnection(connectionString);

            OracleCommand cmd = new OracleCommand(
                "Select " +
                    "SALES_CCN, " +
                    "OTST_NAME, " +
                    "CUSTOMER, " +
                    "Die, " +
                    "Sum(EXT_AMT) As Sales_Amt, " +
                    "Sum(SHIPPED_QTY) As SumSHIPPEDQTY, " +
                    "Sum(ITEM_WEIGHT) As SumITEMWEIGHT " +
                "From " +
                    "(Select " +
                         "SALES_CCN, " +
                         "SO, " +
                         "SO_LINE, " +
                         "AGC, " +
                         "AGC_CODE, " +
                         "ITEM, " +
                         "REVISION, " +
                         "Die, " +
                         "SELL_UM, " +
                         "SO_DELIVERY, " +
                         "SO_SHIPMENT, " +
                         "INVOICE_DATE, " +
                         "SHIPPED_QTY, " +
                         "NET_WEIGHT,  " +
                         "WT, " +
                         "WT_FT, " +
                         "TH_WGT, " +
                         "BOL, " +
                         "STH_WGT, " +
                         "TH_WT_PCT, " +
                         "PCS_NET_WT, " +
                         "ITEM_NET_WT, " +
                         "(Case " +
                             "When SubStr(AGC_CODE, 2) = 'OL' " +
                             "Then 0 " +
                             "When SELL_UM = 'MT' " +
                             "Then ITEM_NET_WT * UM_SCALAR * SHIPPED_QTY " +
                             "When SELL_UM = 'LB' " +
                             "Then SHIPPED_QTY " +
                             "When SELL_UM = 'FT' " +
                             "Then WT_FT * SHIPPED_QTY " +
                             "When SELL_UM = 'IN' " +
                             "Then (WT_FT / 12) * SHIPPED_QTY " +
                             "Else ITEM_NET_WT * SHIPPED_QTY " +
                         "End) As ITEM_WEIGHT, " +
                         "UM_SCALAR, " +
                         "EXT_AMT, " +
                         "AR_DOC, " +
                         "CUSTOMER, " +
                         "OTST_NAME " +
                     "From " +
                          "(Select " +
                               "SALES_CCN, " +
                               "SO, " +
                               "SO_LINE, " +
                               "AGC, " +
                               "AGC_CODE, " +
                               "ITEM, " +
                               "REVISION, " +
                               "Die, " +
                               "SELL_UM, " +
                               "SO_DELIVERY, " +
                               "SO_SHIPMENT, " +
                               "INVOICE_DATE, " +
                               "SHIPPED_QTY, " +
                               "NET_WEIGHT, " +
                               "WT, " +
                               "WT_FT, " +
                               "TH_WGT, " +
                               "BOL, " +
                               "STH_WGT, " +
                               "TH_WT_PCT, " +
                               "PCS_NET_WT, " +
                               "(Case " +
                                   "When SELL_UM = 'EA' " +
                                   "Then (PCS_NET_WT / SHIPPED_QTY) / UM_SCALAR " +
                               "End) As ITEM_NET_WT, " +
                               "UM_SCALAR, " +
                               "EXT_AMT, " +
                               "AR_DOC, " +
                               "CUSTOMER, " +
                               "OTST_NAME " +
                           "From " +
                                "(Select " +
                                     "SALES_CCN, " +
                                     "SO, " +
                                     "SO_LINE, " +
                                     "AGC, " +
                                     "AGC_CODE, " +
                                     "ITEM, " +
                                     "REVISION, " +
                                     "Die, " +
                                     "SELL_UM, " +
                                     "SO_DELIVERY, " +
                                     "SO_SHIPMENT, " +
                                     "INVOICE_DATE, " +
                                     "SHIPPED_QTY, " +
                                     "NET_WEIGHT, " +
                                     "WT, " +
                                     "WT_FT, " +
                                     "TH_WGT, " +
                                     "BOL, " +
                                     "STH_WGT, " +
                                     "(Case " +
                                         "When STH_WGT Is Null " +
                                         "Then 0 " +
                                         "Else (TH_WGT / STH_WGT) " +
                                     "End) As TH_WT_PCT, " +
                                     "(Case " +
                                         "When STH_WGT Is Null " +
                                         "Then 0 " +
                                         "Else NET_WEIGHT * (TH_WGT / STH_WGT) " +
                                     "End) As PCS_NET_WT, " +
                                     "UM_SCALAR, " +
                                     "EXT_AMT, " +
                                     "AR_DOC, " +
                                     "CUSTOMER, " +
                                     "OTST_NAME " +
                                 "From " +
                                      "(Select " +
                                           "SALES_CCN, " +
                                           "SO, " +
                                           "SO_LINE, " +
                                           "AGC, " +
                                           "AGC_CODE, " +
                                           "ITEM, " +
                                           "REVISION, " +
                                           "Die, " +
                                           "SO_DELIVERY, " +
                                           "SO_SHIPMENT, " +
                                           "INVOICE_DATE, " +
                                           "SHIPPED_QTY, " +
                                           "NET_WEIGHT, " +
                                           "WT, " +
                                           "WT_FT, " +
                                           "BOL, " +
                                           "SELL_UM, " +
                                           "UM_SCALAR, " +
                                           "(Case " +
                                               "When SELL_UM = 'MT' " +
                                               "Then WT * UM_SCALAR * SHIPPED_QTY " +
                                               "When SELL_UM = 'LB' " +
                                               "Then SHIPPED_QTY " +
                                               "When SELL_UM = 'FT' " +
                                               "Then WT_FT * SHIPPED_QTY " +
                                               "When SELL_UM = 'IN' " +
                                               "Then (WT_FT / 12) * SHIPPED_QTY " +
                                               "Else WT * SHIPPED_QTY " +
                                           "End) As TH_WGT, " +
                                           "STH_WGT, " +
                                           "AR_DOC, " +
                                           "EXT_AMT, " +
                                           "CUSTOMER, " +
                                           "OTST_NAME " +
                                       "From " +
                                            "(Select " +
                                                 "SO.SALES_CCN, " +
                                                 "SO.SO, " +
                                                 "SO.SO_LINE, " +
                                                 "SO.AGC, " +
                                                 "(Case " +
                                                     "When qryITEM_INFO.AGC Is Null " +
                                                     "Then SO.AGC " +
                                                     "Else qryITEM_INFO.AGC " +
                                                 "End) As AGC_CODE, " +
                                                 "SO.ITEM, " +
                                                 "SO.REVISION, " +
                                                 "qryITEM_INFO.Die, " +
                                                 "SO.SELL_UM, " +
                                                 "SO_DEL.SO_DELIVERY, " +
                                                 "qrySO_SHIP_DTRNG.SO_SHIPMENT, " +
                                                 "qrySO_SHIP_DTRNG.INVOICE_DATE, " +
                                                 "qrySO_SHIP_DTRNG.QTY As SHIPPED_QTY, " +
                                                 "BOL_HDR.NET_WEIGHT, " +
                                                 "(Case " +
                                                     "When qryITEM_INFO.WEIGHT Is Null " +
                                                     "Then 0 " +
                                                     "Else qryITEM_INFO.WEIGHT " +
                                                 "End) As WT, " +
                                                 "(Case " +
                                                     "When qryITEM_INFO.USER1 Is Null " +
                                                     "Then '0' " +
                                                     "Else qryITEM_INFO.USER1 " +
                                                 "End) As WT_FT, " +
                                                 "BOL_HDR.BOL, " +
                                                 "qryBOL_TOTALS.STH_WGT, " +
                                                 "SO.UM_SCALAR, " +
                                                 "SO.UNIT_PRICE, " +
                                                 "qrySO_SHIP_DTRNG.AR_DOC, " +
                                                 "qrySO_SHIP_DTRNG.EXT_AMT, " +
                                                 "SO_HDR.CUSTOMER, " +
                                                 "CUS_LOC.NAME As OTST_NAME " +
                                             "From " +
                                                 "BOL_HDR Inner Join " +
                                                  "(Select " +
                                                       "* " +
                                                   "From " +
                                                       "SO_SHIP " +
                                                   "Where " +
                                                       "SO_SHIP.SALES_CCN = 7 And " +
                                                       "SO_SHIP.INVOICE_DATE Between To_Date('" + begin + "', 'mm/dd/yy') And " +
                                                       "To_Date('" + end + "', 'mm/dd/yy')) qrySO_SHIP_DTRNG On " +
                                                         "BOL_HDR.CCN = qrySO_SHIP_DTRNG.SALES_CCN " +
                                                         "And BOL_HDR.MAS_LOC = qrySO_SHIP_DTRNG.MAS_LOC " +
                                                         "And BOL_HDR.PACKLIST = qrySO_SHIP_DTRNG.PACKLIST Inner Join " +
                                                 "SO_DEL On qrySO_SHIP_DTRNG.SALES_CCN = SO_DEL.SALES_CCN " +
                                                         "And qrySO_SHIP_DTRNG.SO = SO_DEL.SO " +
                                                         "And qrySO_SHIP_DTRNG.SO_LINE = SO_DEL.SO_LINE " +
                                                         "And qrySO_SHIP_DTRNG.SO_DELIVERY = SO_DEL.SO_DELIVERY Inner Join " +
                                                 "SO On SO_DEL.SALES_CCN = SO.SALES_CCN " +
                                                         "And SO_DEL.SO = SO.SO " +
                                                         "And SO_DEL.SO_LINE = SO.SO_LINE Inner Join " +
                                                 "SO_HDR On SO_HDR.SALES_CCN = SO.SALES_CCN " +
                                                         "And SO_HDR.SO = SO.SO Left Join " +
                                                 "CUS_LOC On CUS_LOC.CUSTOMER = SO_HDR.CUSTOMER " +
                                                         "And CUS_LOC.CUS_LOC = SO_HDR.CUS_AR_LOC Left Join " +
                                                  "(Select " +
                                                       "ITEM_CCN.CCN, " +
                                                       "ITEM_CCN.ITEM, " +
                                                       "ITEM_CCN.REVISION, " +
                                                       "ITEM_CCN.AGC, " +
                                                       "ITEM.LENGTH, " +
                                                       "ITEM.USER1, " +
                                                       "ITEM.WEIGHT, " +
                                                       "ITEM_CCN.USER2 As Die " +
                                                   "From " +
                                                       "ITEM Inner Join " +
                                                       "ITEM_CCN On ITEM_CCN.ITEM = ITEM.ITEM " +
                                                               "And ITEM_CCN.REVISION = ITEM.REVISION) qryITEM_INFO On " +
                                                         "qryITEM_INFO.CCN = SO.SALES_CCN " +
                                                         "And qryITEM_INFO.ITEM = SO.ITEM " +
                                                         "And qryITEM_INFO.REVISION = SO.REVISION Inner Join " +
                                                  "(Select " +
                                                       "qryITEM_WEIGHTS.SALES_CCN, " +
                                                       "qryITEM_WEIGHTS.MAS_LOC, " +
                                                       "qryITEM_WEIGHTS.PACKLIST, " +
                                                       "qryITEM_WEIGHTS.BOL, " +
                                                       "Sum(qryITEM_WEIGHTS.TH_WGT) As STH_WGT " +
                                                   "From " +
                                                        "(Select " +
                                                             "SALES_CCN, " +
                                                             "BOL, " +
                                                             "MAS_LOC, " +
                                                             "PACKLIST, " +
                                                             "ITEM, " +
                                                             "REVISION, " +
                                                             "QTY, " +
                                                             "WT, " +
                                                             "WT_FT, " +
                                                             "UM_SCALAR, " +
                                                             "(Case " +
                                                                 "When SELL_UM = 'MT' " +
                                                                 "Then WT * UM_SCALAR * QTY " +
                                                                 "When SELL_UM = 'LB' " +
                                                                 "Then QTY " +
                                                                 "When SELL_UM = 'FT' " +
                                                                 "Then WT_FT * QTY " +
                                                                 "When SELL_UM = 'IN' " +
                                                                 "Then (WT_FT / 12) * QTY " +
                                                                 "Else WT * QTY " +
                                                             "End) As TH_WGT " +
                                                         "From " +
                                                              "(Select " +
                                                                   "SO.SALES_CCN, " +
                                                                   "SO_SHIP.BOL, " +
                                                                   "SO_SHIP.MAS_LOC, " +
                                                                   "SO_SHIP.PACKLIST, " +
                                                                   "SO.ITEM, " +
                                                                   "SO.REVISION, " +
                                                                   "SO_SHIP.QTY, " +
                                                                   "SO.UM_SCALAR, " +
                                                                   "SO.SELL_UM, " +
                                                                   "(Case " +
                                                                       "When ITEM.WEIGHT Is Null " +
                                                                       "Then 0 " +
                                                                       "Else ITEM.WEIGHT " +
                                                                   "End) As WT, " +
                                                                   "(Case " +
                                                                       "When ITEM.USER1 Is Null " +
                                                                       "Then '0' " +
                                                                       "Else ITEM.USER1 " +
                                                                   "End) As WT_FT " +
                                                               "From " +
                                                                   "SO Inner Join " +
                                                                   "SO_DEL On SO_DEL.SALES_CCN = SO.SALES_CCN " +
                                                                           "And SO_DEL.SO = SO.SO " +
                                                                           "And SO_DEL.SO_LINE = SO.SO_LINE Inner Join " +
                                                                   "SO_SHIP On SO_SHIP.SALES_CCN = SO_DEL.SALES_CCN " +
                                                                           "And SO_SHIP.SO = SO_DEL.SO " +
                                                                           "And SO_SHIP.SO_LINE = SO_DEL.SO_LINE " +
                                                                           "And SO_SHIP.SO_DELIVERY = SO_DEL.SO_DELIVERY Left Join " +
                                                                   "ITEM On ITEM.ITEM = SO.ITEM " +
                                                                           "And ITEM.REVISION = SO.REVISION " +
                                                               "Where " +
                                                                   "SO.SALES_CCN = 7)) qryITEM_WEIGHTS " +
                                                   "Group By " +
                                                       "qryITEM_WEIGHTS.SALES_CCN, " +
                                                       "qryITEM_WEIGHTS.MAS_LOC, " +
                                                       "qryITEM_WEIGHTS.PACKLIST, " +
                                                       "qryITEM_WEIGHTS.BOL) qryBOL_TOTALS On qryBOL_TOTALS.SALES_CCN = BOL_HDR.CCN " +
                                                         "And qryBOL_TOTALS.MAS_LOC = BOL_HDR.MAS_LOC " +
                                                         "And qryBOL_TOTALS.PACKLIST = BOL_HDR.PACKLIST " +
                                                         "And qryBOL_TOTALS.BOL = BOL_HDR.BOL " +
                                             "Where " +
                                                 "SO_HDR.CUSTOMER = '" + cxNum + "'))))) qrySO_SHIP_BYACCT " +
                                                 "GROUP BY SALES_CCN, OTST_NAME, CUSTOMER, Die", connection);

            OracleDataAdapter adapter = new OracleDataAdapter(cmd);

            connection.Open();
            adapter.Fill(ds, ds.Tables[3].TableName);
            adapter.Dispose();
            connection.Close();
            connection.Dispose();

            foreach (DataRow row in ds.SalesByDieByAcctNum)
            {
                row[7] = begin;
                row[8] = end;
            }

            ds.AcceptChanges();

            // ProcessingMode will be Either Remote or Local  
            rptViewer.ProcessingMode = ProcessingMode.Local;
            rptViewer.SizeToReportContent = true;
            rptViewer.ZoomMode = ZoomMode.FullPage;                       
            rptViewer.AsyncRendering = false;
            rptViewer.LocalReport.ReportPath = @"C:\Users\tbarrett\source\repos\ExpenseClient\ExpenseClient\Reports\SalesByDieByAcctNum.rdlc";
            ReportDataSource src = new ReportDataSource("SalesByDieByAcctNum", ds.Tables[3]); //@"C:\inetpub\wwwroot\Intranet\Reports\BookingsByAcctNum.rdlc";
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(src);

            ViewBag.ReportViewer = rptViewer;

            return View();
        }

        public ActionResult SalesByAcctByMonth(SalesViewModel model)
        {
            ReportViewer rptViewer = new ReportViewer();
            G2ProdDataSet ds = new G2ProdDataSet();
            DateTime beginn = model.PromiseDateBegin.Date.Date;
            DateTime endd = model.PromiseDateEnd.Date.Date;
            int cxNum = model.CustomerNumber;
            string begin = beginn.Date.ToShortDateString();
            string end = endd.Date.ToShortDateString();

            string connectionString = "Data Source=(DESCRIPTION="
                    + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=gei-glovia)(PORT=1521)))"
                    + "(CONNECT_DATA=(SERVER=prod)(SERVICE_NAME=G2prod.gei.local)));"
                    + "User Id=glovia_prod;Password=manager;";

            OracleConnection connection = new OracleConnection(connectionString);

            OracleCommand cmd = new OracleCommand(
                "Select " +
                        "SALES_CCN, " +
                        "OTST_NAME, " +
                        "CUSTOMER, " +
                        "SumITEMWEIGHT, " +
                        "Sales_Amt, " +
                        "Mth, " +
                        "Yr " +
                    "From " +
                        "(Select " +
                             "qrySO_SHIP_GRP_BYACCT.SALES_CCN, " +
                             "qrySO_SHIP_GRP_BYACCT.OTST_NAME, " +
                             "qrySO_SHIP_GRP_BYACCT.CUSTOMER, " +
                             "qrySO_SHIP_GRP_BYACCT.AR_DOC, " +
                             "qrySO_SHIP_GRP_BYACCT.SumSHIPPEDQTY, " +
                             "ROUND(qrySO_SHIP_GRP_BYACCT.SumITEMWEIGHT, 2) As SumITEMWEIGHT, " +
                             "qryAR_DOC_BYACCTNUM.AMT As Sales_Amt, " +
                             "Extract(Month From qryAR_DOC_BYACCTNUM.DT_) As Mth, " +
                             "Extract(Year From qryAR_DOC_BYACCTNUM.DT_) As Yr " +
                         "From " +
                              "(Select " +
                                   "SALES_CCN, " +
                                   "OTST_NAME, " +
                                   "CUSTOMER, " +
                                   "AR_DOC, " +
                                   "Sum(SHIPPED_QTY) As SumSHIPPEDQTY, " +
                                   "Sum(ITEM_WEIGHT) As SumITEMWEIGHT " +
                               "From " +
                                    "(Select " +
                                         "SALES_CCN, " +
                                         "SO, " +
                                         "SO_LINE, " +
                                         "AGC, " +
                                         "AGC_CODE, " +
                                         "ITEM, " +
                                         "REVISION, " +
                                         "Die, " +
                                         "SELL_UM, " +
                                         "SO_DELIVERY, " +
                                         "SO_SHIPMENT, " +
                                         "INVOICE_DATE, " +
                                         "SHIPPED_QTY, " +
                                         "NET_WEIGHT, " +
                                         "WT, " +
                                         "WT_FT, " +
                                         "TH_WGT, " +
                                         "BOL, " +
                                         "STH_WGT, " +
                                         "TH_WT_PCT, " +
                                         "PCS_NET_WT, " +
                                         "ITEM_NET_WT, " +
                                         "(Case " +
                                             "When SubStr(AGC_CODE, 2) = 'OL' " +
                                             "Then 0 " +
                                             "When SELL_UM = 'MT' " +
                                             "Then ITEM_NET_WT * UM_SCALAR * SHIPPED_QTY " +
                                             "When SELL_UM = 'LB' " +
                                             "Then SHIPPED_QTY " +
                                             "When SELL_UM = 'FT' " +
                                             "Then WT_FT * SHIPPED_QTY " +
                                             "When SELL_UM = 'IN' " +
                                             "Then (WT_FT / 12) * SHIPPED_QTY " +
                                             "Else ITEM_NET_WT * SHIPPED_QTY " +
                                         "End) As ITEM_WEIGHT, " +
                                         "UM_SCALAR, " +
                                         "EXT_AMT, " +
                                         "AR_DOC, " +
                                         "CUSTOMER, " +
                                         "OTST_NAME " +
                                     "From " +
                                          "(Select " +
                                               "SALES_CCN, " +
                                               "SO, " +
                                               "SO_LINE, " +
                                               "AGC, " +
                                               "AGC_CODE, " +
                                               "ITEM, " +
                                               "REVISION, " +
                                               "Die, " +
                                               "SELL_UM, " +
                                               "SO_DELIVERY, " +
                                               "SO_SHIPMENT, " +
                                               "INVOICE_DATE, " +
                                               "SHIPPED_QTY, " +
                                               "NET_WEIGHT, " +
                                               "WT, " +
                                               "WT_FT, " +
                                               "TH_WGT, " +
                                               "BOL, " +
                                               "STH_WGT, " +
                                               "TH_WT_PCT, " +
                                               "PCS_NET_WT, " +
                                               "(Case " +
                                                   "When SELL_UM = 'EA' " +
                                                   "Then (PCS_NET_WT / SHIPPED_QTY) / UM_SCALAR " +
                                               "End) As ITEM_NET_WT, " +
                                               "UM_SCALAR, " +
                                               "EXT_AMT, " +
                                               "AR_DOC, " +
                                               "CUSTOMER, " +
                                               "OTST_NAME " +
                                           "From " +
                                                "(Select " +
                                                     "SALES_CCN, " +
                                                     "SO, " +
                                                     "SO_LINE, " +
                                                     "AGC, " +
                                                     "AGC_CODE, " +
                                                     "ITEM, " +
                                                     "REVISION, " +
                                                     "Die, " +
                                                     "SELL_UM, " +
                                                     "SO_DELIVERY, " +
                                                     "SO_SHIPMENT, " +
                                                     "INVOICE_DATE, " +
                                                     "SHIPPED_QTY, " +
                                                     "NET_WEIGHT, " +
                                                     "WT, " +
                                                     "WT_FT, " +
                                                     "TH_WGT, " +
                                                     "BOL, " +
                                                     "STH_WGT, " +
                                                     "(Case " +
                                                         "When STH_WGT Is Null " +
                                                         "Then 0 " +
                                                         "Else (TH_WGT / STH_WGT) " +
                                                     "End) As TH_WT_PCT, " +
                                                     "(Case " +
                                                         "When STH_WGT Is Null " +
                                                         "Then 0 " +
                                                         "Else NET_WEIGHT * (TH_WGT / STH_WGT) " +
                                                     "End) As PCS_NET_WT, " +
                                                     "UM_SCALAR, " +
                                                     "EXT_AMT, " +
                                                     "AR_DOC, " +
                                                     "CUSTOMER, " +
                                                     "OTST_NAME " +
                                                 "From " +
                                                      "(Select " +
                                                           "SALES_CCN, " +
                                                           "SO, " +
                                                           "SO_LINE, " +
                                                           "AGC, " +
                                                           "AGC_CODE, " +
                                                           "ITEM, " +
                                                           "REVISION, " +
                                                           "Die, " +
                                                           "SO_DELIVERY, " +
                                                           "SO_SHIPMENT, " +
                                                           "INVOICE_DATE, " +
                                                           "SHIPPED_QTY, " +
                                                           "NET_WEIGHT, " +
                                                           "WT, " +
                                                           "WT_FT, " +
                                                           "BOL, " +
                                                           "SELL_UM, " +
                                                           "UM_SCALAR, " +
                                                           "(Case " +
                                                               "When SELL_UM = 'MT' " +
                                                               "Then WT * UM_SCALAR * SHIPPED_QTY " +
                                                               "When SELL_UM = 'LB' " +
                                                               "Then SHIPPED_QTY " +
                                                               "When SELL_UM = 'FT' " +
                                                               "Then WT_FT * SHIPPED_QTY " +
                                                               "When SELL_UM = 'IN' " +
                                                               "Then (WT_FT / 12) * SHIPPED_QTY " +
                                                               "Else WT * SHIPPED_QTY " +
                                                           "End) As TH_WGT, " +
                                                           "STH_WGT, " +
                                                           "AR_DOC, " +
                                                           "EXT_AMT, " +
                                                           "CUSTOMER, " +
                                                           "OTST_NAME " +
                                                       "From " +
                                                            "(Select " +
                                                                 "SO.SALES_CCN, " +
                                                                 "SO.SO, " +
                                                                 "SO.SO_LINE, " +
                                                                 "SO.AGC, " +
                                                                 "(Case " +
                                                                     "When qryITEM_INFO.AGC Is Null " +
                                                                     "Then SO.AGC " +
                                                                     "Else qryITEM_INFO.AGC " +
                                                                 "End) As AGC_CODE, " +
                                                                 "SO.ITEM, " +
                                                                 "SO.REVISION, " +
                                                                 "qryITEM_INFO.Die, " +
                                                                 "SO.SELL_UM, " +
                                                                 "SO_DEL.SO_DELIVERY, " +
                                                                 "qrySO_SHIP_DTRNG.SO_SHIPMENT, " +
                                                                 "qrySO_SHIP_DTRNG.INVOICE_DATE, " +
                                                                 "qrySO_SHIP_DTRNG.QTY As SHIPPED_QTY, " +
                                                                 "BOL_HDR.NET_WEIGHT, " +
                                                                 "(Case " +
                                                                     "When qryITEM_INFO.WEIGHT Is Null " +
                                                                     "Then 0 " +
                                                                     "Else qryITEM_INFO.WEIGHT " +
                                                                 "End) As WT, " +
                                                                 "(Case " +
                                                                     "When qryITEM_INFO.USER1 Is Null " +
                                                                     "Then '0' " +
                                                                     "Else qryITEM_INFO.USER1 " +
                                                                 "End) As WT_FT, " +
                                                                 "BOL_HDR.BOL, " +
                                                                 "qryBOL_TOTALS.STH_WGT, " +
                                                                 "SO.UM_SCALAR, " +
                                                                 "SO.UNIT_PRICE, " +
                                                                 "qrySO_SHIP_DTRNG.AR_DOC, " +
                                                                 "qrySO_SHIP_DTRNG.EXT_AMT, " +
                                                                 "SO_HDR.CUSTOMER, " +
                                                                 "CUS_LOC.NAME As OTST_NAME " +
                                                             "From " +
                                                                 "BOL_HDR Inner Join " +
                                                                  "(Select " +
                                                                       "* " +
                                                                   "From " +
                                                                       "SO_SHIP " +
                                                                   "Where " +
                                                                       "SO_SHIP.SALES_CCN = 7 And " +
                                                                       "SO_SHIP.INVOICE_DATE Between To_Date('" + begin +"', 'mm/dd/yy') And " +
                                                                       "To_Date('" + end + "', 'mm/dd/yy')) qrySO_SHIP_DTRNG On " +
                                                                         "BOL_HDR.CCN = qrySO_SHIP_DTRNG.SALES_CCN " +
                                                                         "And BOL_HDR.MAS_LOC = qrySO_SHIP_DTRNG.MAS_LOC " +
                                                                         "And BOL_HDR.PACKLIST = qrySO_SHIP_DTRNG.PACKLIST Inner Join " +
                                                                 "SO_DEL On qrySO_SHIP_DTRNG.SALES_CCN = SO_DEL.SALES_CCN " +
                                                                         "And qrySO_SHIP_DTRNG.SO = SO_DEL.SO " +
                                                                         "And qrySO_SHIP_DTRNG.SO_LINE = SO_DEL.SO_LINE " +
                                                                         "And qrySO_SHIP_DTRNG.SO_DELIVERY = SO_DEL.SO_DELIVERY Inner Join " +
                                                                 "SO On SO_DEL.SALES_CCN = SO.SALES_CCN " +
                                                                         "And SO_DEL.SO = SO.SO " +
                                                                         "And SO_DEL.SO_LINE = SO.SO_LINE Inner Join " +
                                                                 "SO_HDR On SO_HDR.SALES_CCN = SO.SALES_CCN " +
                                                                         "And SO_HDR.SO = SO.SO Left Join " +
                                                                 "CUS_LOC On CUS_LOC.CUSTOMER = SO_HDR.CUSTOMER " +
                                                                         "And CUS_LOC.CUS_LOC = SO_HDR.CUS_AR_LOC Left Join " +
                                                                  "(Select " +
                                                                       "ITEM_CCN.CCN, " +
                                                                       "ITEM_CCN.ITEM, " +
                                                                       "ITEM_CCN.REVISION, " +
                                                                       "ITEM_CCN.AGC, " +
                                                                       "ITEM.LENGTH, " +
                                                                       "ITEM.USER1, " +
                                                                       "ITEM.WEIGHT, " +
                                                                       "ITEM_CCN.USER2 As Die " +
                                                                   "From " +
                                                                       "ITEM Inner Join " +
                                                                       "ITEM_CCN On ITEM_CCN.ITEM = ITEM.ITEM " +
                                                                               "And ITEM_CCN.REVISION = ITEM.REVISION) qryITEM_INFO " +
                                                                     "On qryITEM_INFO.CCN = SO.SALES_CCN " +
                                                                         "And qryITEM_INFO.ITEM = SO.ITEM " +
                                                                         "And qryITEM_INFO.REVISION = SO.REVISION Inner Join " +
                                                                  "(Select " +
                                                                       "qryITEM_WEIGHTS.SALES_CCN, " +
                                                                       "qryITEM_WEIGHTS.MAS_LOC, " +
                                                                       "qryITEM_WEIGHTS.PACKLIST, " +
                                                                       "qryITEM_WEIGHTS.BOL, " +
                                                                       "Sum(qryITEM_WEIGHTS.TH_WGT) As STH_WGT " +
                                                                   "From " +
                                                                        "(Select " +
                                                                             "SALES_CCN, " +
                                                                             "BOL, " +
                                                                             "MAS_LOC, " +
                                                                             "PACKLIST, " +
                                                                             "ITEM, " +
                                                                             "REVISION, " +
                                                                             "QTY, " +
                                                                             "WT, " +
                                                                             "WT_FT, " +
                                                                             "UM_SCALAR, " +
                                                                             "(Case " +
                                                                                 "When SELL_UM = 'MT' " +
                                                                                 "Then WT * UM_SCALAR * QTY " +
                                                                                 "When SELL_UM = 'LB' " +
                                                                                 "Then QTY " +
                                                                                 "When SELL_UM = 'FT' " +
                                                                                 "Then WT_FT * QTY " +
                                                                                 "When SELL_UM = 'IN' " +
                                                                                 "Then (WT_FT / 12) * QTY " +
                                                                                 "Else WT * QTY " +
                                                                             "End) As TH_WGT " +
                                                                         "From " +
                                                                              "(Select " +
                                                                                   "SO.SALES_CCN, " +
                                                                                   "SO_SHIP.BOL, " +
                                                                                   "SO_SHIP.MAS_LOC, " +
                                                                                   "SO_SHIP.PACKLIST, " +
                                                                                   "SO.ITEM, " +
                                                                                   "SO.REVISION, " +
                                                                                   "SO_SHIP.QTY, " +
                                                                                   "SO.UM_SCALAR, " +
                                                                                   "SO.SELL_UM, " +
                                                                                   "(Case " +
                                                                                       "When ITEM.WEIGHT Is Null " +
                                                                                       "Then 0 " +
                                                                                       "Else ITEM.WEIGHT " +
                                                                                   "End) As WT, " +
                                                                                   "(Case " +
                                                                                       "When ITEM.USER1 Is Null " +
                                                                                       "Then '0' " +
                                                                                       "Else ITEM.USER1 " +
                                                                                   "End) As WT_FT " +
                                                                               "From " +
                                                                                   "SO Inner Join " +
                                                                                   "SO_DEL On SO_DEL.SALES_CCN = SO.SALES_CCN " +
                                                                                           "And SO_DEL.SO = SO.SO " +
                                                                                           "And SO_DEL.SO_LINE = SO.SO_LINE Inner Join " +
                                                                                   "SO_SHIP On SO_SHIP.SALES_CCN = SO_DEL.SALES_CCN " +
                                                                                           "And SO_SHIP.SO = SO_DEL.SO " +
                                                                                           "And SO_SHIP.SO_LINE = SO_DEL.SO_LINE " +
                                                                                           "And SO_SHIP.SO_DELIVERY = SO_DEL.SO_DELIVERY " +
                                                                                   "Left Join " +
                                                                                   "ITEM On ITEM.ITEM = SO.ITEM " +
                                                                                           "And ITEM.REVISION = SO.REVISION " +
                                                                               "Where " +
                                                                                   "SO.SALES_CCN = 7)) qryITEM_WEIGHTS " +
                                                                   "Group By " +
                                                                       "qryITEM_WEIGHTS.SALES_CCN, " +
                                                                       "qryITEM_WEIGHTS.MAS_LOC, " +
                                                                       "qryITEM_WEIGHTS.PACKLIST, " +
                                                                       "qryITEM_WEIGHTS.BOL) qryBOL_TOTALS On qryBOL_TOTALS.SALES_CCN = " +
                                                                         "BOL_HDR.CCN " +
                                                                         "And qryBOL_TOTALS.MAS_LOC = BOL_HDR.MAS_LOC " +
                                                                         "And qryBOL_TOTALS.PACKLIST = BOL_HDR.PACKLIST " +
                                                                         "And qryBOL_TOTALS.BOL = BOL_HDR.BOL " +
                                                             "Where " +
                                                                 "SO_HDR.CUSTOMER = '" + cxNum + "'))))) " +
                               "Group By " +
                                   "SALES_CCN, " +
                                   "OTST_NAME, " +
                                   "CUSTOMER, " +
                                   "AR_DOC) qrySO_SHIP_GRP_BYACCT Inner Join " +
                              "(Select " +
                                   "AR_DOC.AR_CCN, " +
                                   "AR_DOC.AR_DOC_TYPE, " +
                                   "AR_DOC.AR_DOC, " +
                                   "AR_DOC.CCN, " +
                                   "AR_DOC.AGC, " +
                                   "AR_DOC.AMT, " +
                                   "AR_DOC.CM_APPLIED_AMT, " +
                                   "AR_DOC.BOOK_RATE, " +
                                   "AR_DOC.CREATED_DATE, " +
                                   "AR_DOC.CURRENCY, " +
                                   "AR_DOC.CUS_PO, " +
                                   "AR_DOC.DT_, " +
                                   "AR_DOC.DISC_AMT, " +
                                   "AR_DOC.DISC_APPLIED_AMT, " +
                                   "AR_DOC.DISC_DUE_DATE, " +
                                   "AR_DOC.DISC_EXCEPTION_AMT, " +
                                   "AR_DOC.DISC_GRACE_DAYS, " +
                                   "AR_DOC.DISC_PCT, " +
                                   "AR_DOC.FROZEN_DATE, " +
                                   "AR_DOC.NET_DUE_DATE, " +
                                   "AR_DOC.PAID_AMT, " +
                                   "AR_DOC.PAID_DATE, " +
                                   "AR_DOC.ORIG_BOOK_RATE, " +
                                   "AR_DOC.REASON, " +
                                   "AR_DOC.REVAL_DATE, " +
                                   "AR_DOC.SOURCE, " +
                                   "AR_DOC.STMT_DATE, " +
                                   "AR_DOC.TERMS_CODE, " +
                                   "AR_DOC.VAT_AMT, " +
                                   "AR_DOC.VAT_BASE_AMT, " +
                                   "AR_DOC.VAT_REG, " +
                                   "AR_DOC.WRITE_OFF_AMT, " +
                                   "AR_DOC.WRITE_OFF_REASON, " +
                                   "AR_DOC.NOTES_REC, " +
                                   "AR_DOC.DISC_OVERRIDDEN, " +
                                   "AR_DOC.VAT_EXEMPT, " +
                                   "AR_DOC.SCHED_PMT, " +
                                   "AR_DOC.LAST_VAR_SEQ, " +
                                   "AR_DOC.LAST_PMT_SEQ, " +
                                   "AR_DOC.BANK, " +
                                   "AR_DOC.AR_DEP_SEQ, " +
                                   "AR_DOC.AR_DEP_LINE, " +
                                   "AR_DOC.UNAPPL_CASH_MEMO, " +
                                   "AR_DOC.BOOK_RATE_DIV, " +
                                   "AR_DOC.ORIG_BOOK_RATE_DIV, " +
                                   "AR_DOC.VAT_EXEMPT_REASON, " +
                                   "AR_DOC.CUSTOMER, " +
                                   "AR_DOC.CUS_AR_LOC, " +
                                   "AR_DOC.AR_DEP_DATE, " +
                                   "AR_DOC.NET_DUE_ADATE, " +
                                   "AR_DOC.WRITEOFF_APPROVER_NAME, " +
                                   "AR_DOC.CONTRA_AMT, " +
                                   "AR_DOC.CONTRA_NO, " +
                                   "AR_DOC.AI, " +
                                   "AR_DOC.DISPUTED, " +
                                   "AR_DOC.REVERSE_DATE, " +
                                   "AR_DOC.REVERSE_REASON, " +
                                   "AR_DOC.REVERSE_MEMO, " +
                                   "AR_DOC.PRINT_NOTES, " +
                                   "AR_DOC.REVERSE_ASOF_DATE, " +
                                   "AR_DOC.WRITE_OFF_ACCT, " +
                                   "AR_DOC.TRANSACTION_TYPE_CODE, " +
                                   "AR_DOC.VATPCD_CODE, " +
                                   "AR_DOC.DISPATCH_VAT_REG, " +
                                   "AR_DOC.LAST_ASOF_DATE, " +
                                   "AR_DOC.LAST_TRANS_RATE, " +
                                   "AR_DOC.LAST_TRANS_RATE_DATE, " +
                                   "AR_DOC.LAST_TRANS_RATE_DIV, " +
                                   "AR_DOC.LAST_TRANS_TYPE, " +
                                   "AR_DOC.LAST_APPLIED_DATE, " +
                                   "AR_DOC.USER_ALPHA1, " +
                                   "AR_DOC.USER_ALPHA2, " +
                                   "AR_DOC.USER_ALPHA3, " +
                                   "AR_DOC.USER_NUM1, " +
                                   "AR_DOC.USER_NUM2, " +
                                   "AR_DOC.USER_NUM3, " +
                                   "AR_DOC.USER_DATE, " +
                                   "AR_DOC.USER_TIME, " +
                                   "AR_DOC.IN_PAYMENT, " +
                                   "AR_DOC.REFUND_AMT, " +
                                   "AR_DOC.REFUND_DATE, " +
                                   "AR_DOC.SEL_PMT_AMT, " +
                                   "AR_DOC.AR_CRDET_SEQ, " +
                                   "AR_DOC.BASE_DISC_AMT, " +
                                   "AR_DOC.BASE_PAYMENT_AMT, " +
                                   "AR_DOC.CHG_E7_AMT, " +
                                   "AR_DOC.E7_AMT, " +
                                   "AR_DOC.LAST_APPLIED_ACDET_SEQ, " +
                                   "AR_DOC.LAST_APPLIED_DOC, " +
                                   "AR_DOC.LAST_APPLIED_DOC_TYPE, " +
                                   "AR_DOC.LAST_WOFF_SEQ " +
                               "From " +
                                   "AR_DOC " +
                               "Where " +
                                   "AR_DOC.CCN = 7 And " +
                                   "AR_DOC.AR_DOC_TYPE = 'I' And " +
                                   "AR_DOC.DT_ Between To_Date('" + begin + "', 'mm/dd/yy') And To_Date('" + end + "', 'mm/dd/yy') And " +
                                   "AR_DOC.CUSTOMER = '" + cxNum + "') qryAR_DOC_BYACCTNUM On qryAR_DOC_BYACCTNUM.AR_CCN = " +
                                     "qrySO_SHIP_GRP_BYACCT.SALES_CCN " +
                                     "And qryAR_DOC_BYACCTNUM.AR_DOC = qrySO_SHIP_GRP_BYACCT.AR_DOC) qrySalesAndLbsByMonth", connection);

            OracleDataAdapter adapter = new OracleDataAdapter(cmd);

            connection.Open();
            adapter.Fill(ds, ds.Tables[2].TableName);
            adapter.Dispose();
            connection.Close();
            connection.Dispose();

            foreach (DataRow row in ds.SalesByAcctByMonth)
            {
                row[7] = begin;
                row[8] = end;
            }

            ds.AcceptChanges();

            // ProcessingMode will be Either Remote or Local  
            rptViewer.ProcessingMode = ProcessingMode.Local;
            rptViewer.SizeToReportContent = true;
            rptViewer.ZoomMode = ZoomMode.FullPage;
            // rptViewer.Width = Unit.Percentage(99);
            // rptViewer.Height = Unit.Pixel(1000);                
            rptViewer.AsyncRendering = false;
            rptViewer.LocalReport.ReportPath = @"C:\Users\tbarrett\source\repos\ExpenseClient\ExpenseClient\Reports\SalesByAcctByMonth.rdlc";
            ReportDataSource src = new ReportDataSource("SalesByAcctByMonth", ds.Tables[2]); //@"C:\inetpub\wwwroot\Intranet\Reports\BookingsByAcctNum.rdlc";
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(src);

            ViewBag.ReportViewer = rptViewer;
            return View();            
        }

        public ActionResult BookingsByAcctNum(SalesViewModel model)
        {
            ReportViewer rptViewer = new ReportViewer();
            G2ProdDataSet ds = new G2ProdDataSet();
            DateTime beginn = model.PromiseDateBegin.Date.Date;
            DateTime endd = model.PromiseDateEnd.Date.Date;
            int cxNum = model.CustomerNumber;
            string begin = beginn.Date.ToShortDateString();
            string end = endd.Date.ToShortDateString();

            string connectionString = "Data Source=(DESCRIPTION="
                    + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=gei-glovia)(PORT=1521)))"
                    + "(CONNECT_DATA=(SERVER=prod)(SERVICE_NAME=G2prod.gei.local)));"
                    + "User Id=glovia_prod;Password=manager;";

            OracleConnection connection = new OracleConnection(connectionString);
                
            OracleCommand cmd = new OracleCommand(
                "Select " +
                    "ROUND(Sum(OPEN_QTY), 2) As SOPEN_QTY, " +
                    "OTST_NAME, " +
                    "CUSTOMER, " +
                    "ROUND(Sum(OPEN_QTY * UNIT_PRICE), 2) As SALES_AMT, " +
                    "ROUND(Sum(Case " +
                        "When SELL_UM = 'MT' " +
                        "Then WT * UM_SCALAR * OPEN_QTY " +
                        "When SELL_UM = 'LB' " +
                        "Then OPEN_QTY " +
                        "When SELL_UM = 'FT' " +
                        "Then WT_FT * OPEN_QTY " +
                        "When SELL_UM = 'IN' " +
                        "Then (WT_FT / 12) * OPEN_QTY " +
                        "Else WT * OPEN_QTY " +
                    "End), 2) As TH_WGT " +
                "From " +
                    "(Select " +
                         "SO_HDR.SALES_CCN As SALES_CCN, " +
                         "SO.UNIT_PRICE As UNIT_PRICE, " +
                         "SO.ITEM As ITEM, " +
                         "SO.REVISION, " +
                         "SO.SELL_UM As SELL_UM, " +
                         "SO_DEL.SO_DELIVERY, " +
                         "(SO_DEL.ORDERED_QTY - SO_DEL.TOT_SHIP_QTY - SO_DEL.CANC_QTY) As OPEN_QTY, " +
                         "SO_DEL.PROMISE_DATE As PROMISE_DATE, " +
                         "SO.UM_SCALAR, " +
                         "SO_HDR.CUSTOMER As CUSTOMER, " +
                         "CUS_LOC.NAME As OTST_NAME, " +
                         "(Case " +
                             "When ITEM.WEIGHT Is Null " +
                             "Then 0 " +
                             "Else ITEM.WEIGHT " +
                         "End) As WT, " +
                         "(Case " +
                             "When ITEM.USER1 Is Null " +
                             "Then '0' " +
                             "Else ITEM.USER1 " +
                         "End) As WT_FT " +
                     "From " +
                         "SO Inner Join " +
                         "SO_DEL On SO_DEL.SALES_CCN = SO.SALES_CCN " +
                                 "And SO_DEL.SO = SO.SO " +
                                 "And SO_DEL.SO_LINE = SO.SO_LINE Inner Join " +
                         "SO_HDR On SO_HDR.SALES_CCN = SO.SALES_CCN " +
                                 "And SO_HDR.SO = SO.SO Left Join " +
                         "CUS_LOC On CUS_LOC.CUSTOMER = SO_HDR.CUSTOMER " +
                                 "And CUS_LOC.CUS_LOC = SO_HDR.CUS_AR_LOC Left Join " +
                         "ITEM On ITEM.ITEM = SO.ITEM " +
                                 "And ITEM.REVISION = SO.REVISION) " +
                "Where " +
                    "PROMISE_DATE BETWEEN to_date('" + begin + "', 'mm/dd/yyyy') AND to_date('" + end + "', 'mm/dd/yyyy') And " +
                    "OPEN_QTY > 0 And " +
                    "SALES_CCN = 7 And CUSTOMER = " + cxNum + " " +                   
                "Group By " +
                    "OTST_NAME, " +
                    "CUSTOMER", connection); 

            OracleDataAdapter adapter = new OracleDataAdapter(cmd);

            connection.Open();
            adapter.Fill(ds, ds.Tables[0].TableName);
            adapter.Dispose();
            connection.Close();
            connection.Dispose();
            

            foreach (DataRow row in ds.bookingsbyacct)
            {
                row[5] = begin;
                row[6] = end;
            }

            ds.AcceptChanges();

            // ProcessingMode will be Either Remote or Local  
            rptViewer.ProcessingMode = ProcessingMode.Local;
            rptViewer.SizeToReportContent = true;
            rptViewer.ZoomMode = ZoomMode.FullPage;
            // rptViewer.Width = Unit.Percentage(99);
            // rptViewer.Height = Unit.Pixel(1000);                
            rptViewer.AsyncRendering = false;
            rptViewer.LocalReport.ReportPath = @"C:\Users\tbarrett\source\repos\ExpenseClient\ExpenseClient\Reports\BookingsByAcctNum.rdlc";
            ReportDataSource src = new ReportDataSource("BookingsByAcctNum", ds.Tables[0]); //@"C:\inetpub\wwwroot\Intranet\Reports\BookingsByAcctNum.rdlc";
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(src);

            ViewBag.ReportViewer = rptViewer;
            return View();
            
        }

        public ActionResult BookingsByCustomer(SalesViewModel model)
        {
            ReportViewer rptViewer = new ReportViewer();
            G2ProdDataSet ds = new G2ProdDataSet();
            DateTime beginn = model.PromiseDateBegin.Date.Date;
            DateTime endd = model.PromiseDateEnd.Date.Date;

            string begin = beginn.Date.ToShortDateString();
            string end = endd.Date.ToShortDateString();

            string connectionString = "Data Source=(DESCRIPTION="
                    + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=gei-glovia)(PORT=1521)))"
                    + "(CONNECT_DATA=(SERVER=prod)(SERVICE_NAME=G2prod.gei.local)));"
                    + "User Id=glovia_prod;Password=manager;";
            OracleConnection connection = new OracleConnection(connectionString);

            OracleCommand cmd = new OracleCommand(
               "Select " +
                  "ROUND(Sum(OPEN_QTY), 2) As SOPEN_QTY, " +
                  "OTST_NAME, " +
                  "CUSTOMER, " +
                  "ROUND(Sum(OPEN_QTY * UNIT_PRICE), 2) As SALES_AMT, " +
                  "ROUND(Sum(Case " +
                      "When SELL_UM = 'MT' " +
                      "Then WT * UM_SCALAR * OPEN_QTY " +
                      "When SELL_UM = 'LB' " +
                      "Then OPEN_QTY " +
                      "When SELL_UM = 'FT' " +
                      "Then WT_FT * OPEN_QTY " +
                      "When SELL_UM = 'IN' " +
                      "Then (WT_FT / 12) * OPEN_QTY " +
                      "Else WT * OPEN_QTY " +
                     "End), 2) As TH_WGT, " +
                     "(Case " +
                      "When USER_ALPHA1 Is Null " +
                      "Then CUSTOMER " +
                      "Else USER_ALPHA1 " +
                     "End) As PRIM_CUST " +
                "From " +
                  "(Select " +
                      "SO_HDR.SALES_CCN As SALES_CCN, " +
                      "SO.ITEM As ITEM, " +
                      "SO.REVISION, " +
                      "SO.SELL_UM As SELL_UM, " +
                      "SO_DEL.SO_DELIVERY, " +
                      "SO_DEL.PROMISE_DATE As PROMISE_DATE, " +
                      "SO.UM_SCALAR As UM_SCALAR, " +
                      "SO_HDR.CUSTOMER As CUSTOMER, " +
                      "CUS_LOC.USER_ALPHA1 As USER_ALPHA1, " +
                      "SO.UNIT_PRICE As UNIT_PRICE, " +
                      "CUS_LOC.CUS_LOC As CUS_LOC, " +
                      "(SO_DEL.ORDERED_QTY - SO_DEL.TOT_SHIP_QTY - SO_DEL.CANC_QTY) As OPEN_QTY, " +
                      "CUS_LOC.NAME As OTST_NAME, " +
                      "(Case " +
                        "When ITEM.WEIGHT Is Null " +
                        "Then 0 " +
                        "Else ITEM.WEIGHT " +
                       "End) As WT, " +
                      "(Case " +
                        "When ITEM.USER1 Is Null " +
                        "Then '0' " +
                        "Else ITEM.USER1 " +
                       "End) As WT_FT " +
                  "From " +
                      "SO Inner Join " +
                      "SO_DEL On SO_DEL.SALES_CCN = SO.SALES_CCN " +
                              "And SO_DEL.SO = SO.SO " +
                              "And SO_DEL.SO_LINE = SO.SO_LINE Inner Join " +
                      "SO_HDR On SO_HDR.SALES_CCN = SO.SALES_CCN " +
                              "And SO_HDR.SO = SO.SO Left Join " +
                      "ITEM On ITEM.ITEM = SO.ITEM " +
                              "And ITEM.REVISION = SO.REVISION Left Join " +
                      "CUS_LOC On CUS_LOC.CUSTOMER = SO_HDR.CUSTOMER) " +
                "Where " +
                  "PROMISE_DATE BETWEEN to_date('" + begin + "', 'mm/dd/yyyy') AND to_date('" + end + "', 'mm/dd/yyyy') And " +
                  "OPEN_QTY >= 1 and SALES_CCN = 7 and CUS_LOC = ' '" +
                "GROUP BY " +
                  "OTST_NAME, " + 
                  "CUSTOMER, " +
                  "USER_ALPHA1 " +
                  "ORDER BY " + 
                  "OTST_NAME", connection); 

            OracleDataAdapter adapter = new OracleDataAdapter(cmd);
            
            
            connection.Open();
            adapter.Fill(ds, ds.Tables[1].TableName);
            connection.Close();
            connection.Dispose();

            foreach (DataRow row in ds.bookingsbycustomer)
            {
                row[6] = begin;
                row[7] = end;
            }
            
            ds.AcceptChanges();
            
            // ProcessingMode will be Either Remote or Local  
            rptViewer.ProcessingMode = ProcessingMode.Local;
            rptViewer.SizeToReportContent = true;
            rptViewer.ZoomMode = ZoomMode.FullPage;
            // rptViewer.Width = Unit.Percentage(99);
            // rptViewer.Height = Unit.Pixel(1000);                
            rptViewer.AsyncRendering = false;

            rptViewer.LocalReport.ReportPath = @"C:\Users\tbarrett\source\repos\ExpenseClient\ExpenseClient\Reports\BookingsByCustomer.rdlc"; //@"C:\inetpub\wwwroot\Intranet\Reports\BookingsByCustomer.rdlc";                
            ReportDataSource src = new ReportDataSource("BookingsByCustomer", ds.Tables[1]);
            rptViewer.LocalReport.DataSources.Clear();     
            rptViewer.LocalReport.DataSources.Add(src);


            ViewBag.ReportViewer = rptViewer;
            return View();
        }

        
    }
}