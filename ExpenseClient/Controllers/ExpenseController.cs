﻿using ExpenseClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExpenseClient.ViewModels;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Mapping;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure.MappingViews;
using System.Threading.Tasks;
using AutoMapper.Mappers.Internal;
using AutoMapper.Features;
using AutoMapper.Execution;

namespace ExpenseClient.Controllers
{
    public class ExpenseController : Controller
    {
        private GEIConnection db = new GEIConnection();
        
        
        public List<CatDropDown> GetDetails() //populate dropdown and model bind
        {
            GEIConnection _context = new GEIConnection();
            List<CatDropDown> results = new List<CatDropDown>();
            var obj = _context.tblExpenseTables.Select(m => m).ToList();
            if (obj != null && obj.Count() > 0)
            {
                foreach (var data in obj)
                {
                    CatDropDown model = new CatDropDown();
                    model.Categoryname = data.CategoryName;
                    model.CategoryID = data.CategoryID;
                    model.Accountnum = data.AccountNum;
                    results.Add(model);
                }
            }

            return results;
        }
        
        // GET: Expense        
        public ActionResult Index()
        {
            EmployeeConnection db = new EmployeeConnection();

            string user = User.Identity.Name.Substring(9);
            
            ExpenseViewModel model = new ExpenseViewModel();
            model.CategoryDropDownProperty = GetDetails();            
            model.EmployeeName = user;
            model.EmployeeNum = db.tblEmployees
                .Where(s => s.UName == user)
                .Select(s => s.EmpBdgeNum).Single();
            
            return View(model);

        }
       
        [HttpPost]
        public ActionResult Index(ExpenseViewModel model) //overloaded index method
        {
            //validation
            if (model.categoryid == "2S" && model.Amount > 125)
            {
                return View("PhoneError");
            }
            if (model.categoryid == "3A" && model.Amount > model.Attendees * 10)
            {
                return View("MealError");
            }
            if (model.categoryid == "3B" && model.Amount > model.Attendees * 15)
            {
                return View("MealError");
            }
            if (model.categoryid == "3C" && model.Amount > model.Attendees * 25)
            {
                return View("MealError");
            }

            GEIConnection db = new GEIConnection();
            tblExpenseDetail dbo = new tblExpenseDetail();
            tblExpenseBasic dbheader = new tblExpenseBasic();

            string name = model.EmployeeName;
            string num = model.categoryid;

            var context = new EmployeeConnection();

            //week ending method
            DayOfWeek day = DateTime.Now.DayOfWeek;
            int days = day - DayOfWeek.Monday;
            DateTime start = DateTime.Now.AddDays(-days);
            DateTime weekEndDate = start.AddDays(6);

            //Header insert
            dbheader.EmployeeNum = context.tblEmployees
                .Where(m => m.UName == name)
                .Select(m => m.EmpBdgeNum).Single();
            dbheader.WeekEndingDate = weekEndDate;
            dbheader.Date = DateTime.Now;
            dbheader.CashAdvance = model.CashAdvance;
            dbheader.AdvanceAmount = model.AdvanceAmt;

            decimal rate = 0.555m;
            decimal? miles = model.Mileage;
            
            //Mileage calculation
            if (model.categoryid == "1")
            {
                model.Amount = miles * rate;

            }


            dbo.CategoryID = model.categoryid; //assigning the model properties to the table object                       
            dbo.ExpenseDate = model.ExpenseDate;            
            dbo.Mileage = model.Mileage;
            dbo.Amount = model.Amount;
            dbo.AccountNum = db.tblExpenseTables
                .Where(s => s.CategoryID == num)
                .Select(s => s.AccountNum).Single();
            dbo.Reimburse = model.Reimburse;
            dbo.NumOfAttendees = model.Attendees;
            dbo.BusinessPurpose = model.Purpose;
            dbo.PersonsEntertained = model.PersonsEntertained;
            dbo.PlaceCityState = model.Place;
            dbo.Name = model.EmployeeName;
            dbo.Date = DateTime.Now;
            dbo.WeekEndingDate = WeekEnd(model.ExpenseDate);
            dbo.EmployeeNum = context.tblEmployees
                .Where(m => m.UName == name)
                .Select(m => m.EmpBdgeNum).Single();


            if (ModelState.IsValid)
            {
                
                db.tblExpenseDetails.Add(dbo); //adding the table objects to record
                db.tblExpenseBasics.Add(dbheader);
                db.SaveChanges();

                return RedirectToAction("Index"); //redirect to non-overloaded index
            }
            else
            {
                return View(model);
            }
        }


        [HttpGet]
        public JsonResult LoadData()
        {
            //week ending method
            DayOfWeek day = DateTime.Now.DayOfWeek;
            int days = day - DayOfWeek.Monday;
            DateTime start = DateTime.Now.AddDays(-days);
            DateTime weekEndDate = start.AddDays(6);

            DateTime now = DateTime.Now;

            using (GEIConnection dc = new GEIConnection())
            {
                string user = User.Identity.Name.Substring(9);
                ExpenseViewModel model = new ExpenseViewModel();
                model.CategoryDropDownProperty = GetDetails();
                List<ExpenseViewModel> List = dc.tblExpenseDetails
                            .Where(o => o.Name == user && o.WeekEndingDate == weekEndDate.Date)
                            .ToList()
                            .Select(o => new ExpenseViewModel
                            {
                                categoryid = o.CategoryID,
                                ExpenseDate = o.ExpenseDate,
                                Amount = o.Amount,
                                Mileage = o.Mileage,
                                Line = o.Line,
                                EmployeeName = o.Name,
                                EmployeeNum = o.EmployeeNum,
                                Attendees = o.NumOfAttendees,
                                Place = o.PlaceCityState,
                                PersonsEntertained = o.PersonsEntertained,
                                Purpose = o.BusinessPurpose,
                                Reimburse = o.Reimburse
                            }).ToList();

                return Json(List, JsonRequestBehavior.AllowGet);
            }

        }


        public DateTime WeekEnd(DateTime? sel)
        {
            //week ending method            
             
            DayOfWeek day = sel.Value.Date.DayOfWeek;
            int days = day - DayOfWeek.Monday;
            DateTime start = sel.Value.AddDays(-days);
            DateTime weekEndDate = start.AddDays(6);

            return weekEndDate.Date;
        }

        [HttpPost]
        public ActionResult Edit(ExpenseViewModel model)
        {
            GEIConnection db = new GEIConnection();
            //week ending method
            DayOfWeek day = DateTime.Now.DayOfWeek;
            int days = day - DayOfWeek.Monday;
            DateTime start = DateTime.Now.AddDays(-days);
            DateTime weekEndDate = start.AddDays(6);
            
            string num = model.categoryid;

            try
            {              

                //Update
                var asset = db.tblExpenseDetails.SingleOrDefault(x => x.Line == model.Line);
                
                asset.CategoryID = model.categoryid;
                
                asset.ExpenseDate = model.ExpenseDate;
                asset.Amount = model.Amount;
                asset.Mileage = model.Mileage;
                asset.WeekEndingDate = WeekEnd(model.ExpenseDate);
                asset.BusinessPurpose = model.Purpose;
                asset.EmployeeNum = model.EmployeeNum;
                asset.Name = model.EmployeeName;
                asset.Date = DateTime.Now;
                asset.NumOfAttendees = model.Attendees;
                asset.PlaceCityState = model.Place;
                asset.PersonsEntertained = model.PersonsEntertained;
                asset.Reimburse = model.Reimburse;
                asset.AccountNum = db.tblExpenseTables
                .Where(s => s.CategoryID == num)
                .Select(s => s.AccountNum).Single();
                asset.Line = model.Line;
                db.SaveChanges();

                
                
                return RedirectToAction("Index");
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        
        public ActionResult EditExpense(int? Line)
        {
            GEIConnection db = new GEIConnection();
            List<tblExpenseDetail> list = db.tblExpenseDetails.ToList();
            ViewBag.DepartmentList = list;

            ExpenseViewModel model = new ExpenseViewModel();
            model.CategoryDropDownProperty = GetDetails();
            if (Line > 0)
            {

                tblExpenseDetail emp = db.tblExpenseDetails.SingleOrDefault(x => x.Line == Line);
                model.ExpenseDate = emp.ExpenseDate;
                model.categoryid = emp.CategoryID;
                model.Amount = emp.Amount;
                model.Mileage = emp.Mileage;
                model.EmployeeName = emp.Name;
                model.EmployeeNum = emp.EmployeeNum;
                model.Attendees = emp.NumOfAttendees;
                model.Place = emp.PlaceCityState;
                model.PersonsEntertained = emp.PersonsEntertained;
                model.Purpose = emp.BusinessPurpose;
                model.Reimburse = emp.Reimburse;

            }
            return PartialView("EditExpense", model);

        }

        [HttpPost]
        public ActionResult delete(int? Line)
        {
            GEIConnection db = new GEIConnection();
            tblExpenseDetail emp = db.tblExpenseDetails.SingleOrDefault(x => x.Line == Line);
            if (Line > 0)
            {
                db.tblExpenseDetails.Remove(emp);
                db.SaveChanges();
            }
            else
            {
                //do nothing
            }

            return Json("success");

        }
        [HttpGet]
        public JsonResult Populate(string time)
        {
            DateTime t = DateTime.Parse(time);
            DateTime e = WeekEnd(t);
            GEIConnection dc = new GEIConnection();
            string user = User.Identity.Name.Substring(9);
            ExpenseViewModel model = new ExpenseViewModel();
            model.CategoryDropDownProperty = GetDetails();

            List<ExpenseViewModel> list = dc.tblExpenseDetails
                        .Where(o => o.WeekEndingDate == e.Date && o.Name == user)
                        .ToList()
                        .Select(o => new ExpenseViewModel
                        {
                            categoryid = o.CategoryID,
                            ExpenseDate = o.ExpenseDate,
                            Amount = o.Amount,
                            Mileage = o.Mileage,
                            Line = o.Line,
                            EmployeeName = o.Name,
                            EmployeeNum = o.EmployeeNum,
                            Attendees = o.NumOfAttendees,
                            Place = o.PlaceCityState,
                            PersonsEntertained = o.PersonsEntertained,
                            Purpose = o.BusinessPurpose,
                            Reimburse = o.Reimburse
                        }).ToList();


            return Json(list, JsonRequestBehavior.AllowGet);
        }
        
       
        public ActionResult ModalPopUpp()
        {
            return PartialView("EditPrevious");
        }
        
        public ActionResult Previous()
        {
            return View();
        }


    }
}