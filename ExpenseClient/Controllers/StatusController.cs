﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ExpenseClient.Models;

namespace ExpenseClient.Controllers
{
    public class StatusController : ApiController
    {
       
        /// <summary>
        /// GET: Work Order Status by ID
        /// </summary>
        /// <param name="id">Work Order Number</param>        
        /// <returns>Current status of Work order</returns>
        // GET: api/Status/wo=?line=?        
        public string Get(int id)
        {
            GEI_IS_WOEntities db = new GEI_IS_WOEntities();
            var obj = db.tblMains
                .Where(m => m.WorkOrderNum == id)
                .Select(m => m.Status).Single();
                
            return "Work Order Status:" + obj;
        }

        /// <summary>
        /// GET: Open IS Work Orders
        /// </summary>
        /// <returns>Table data for open IS Work Orders</returns>
        [Route("api/Status/GetOrders")]
        [HttpGet]
        // GET: api/Status/GetOrders
        public List<tblMain> GetOrders()
        {
            GEI_IS_WOEntities db = new GEI_IS_WOEntities();
            List<tblMain> obj = db.tblMains.Where(m => m.Status != "Closed").ToList();

            return obj;
        }

        
        
    }
}
