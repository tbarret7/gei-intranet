﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExpenseClient.ViewModels;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;
using ExpenseClient.Models;

namespace ExpenseClient.Controllers
{
    public class ExpenseReportingController : Controller
    {
        // GET: ExpenseReporting
        public ActionResult Index()
        {
            
            return View();
        }
        public DateTime WeekEnd(DateTime? sel)
        {
            //week ending method

            DayOfWeek day = sel.Value.Date.DayOfWeek;
            int days = day - DayOfWeek.Monday;
            DateTime start = sel.Value.AddDays(-days);
            DateTime weekEndDate = start.AddDays(6);

            return weekEndDate.Date;
        }

        public ActionResult Report()
        {
            ReportViewer rptViewer = new ReportViewer();
            GEI_Expense_ClientDataSet ds = new GEI_Expense_ClientDataSet();
            GEIConnection db = new GEIConnection();
            
            //week ending method
            DayOfWeek day = DateTime.Now.DayOfWeek;
            int days = day - DayOfWeek.Monday;
            DateTime start = DateTime.Now.AddDays(-days);
            DateTime weekEndDate = start.AddDays(6);
            //User Parse
            string user = User.Identity.Name.Substring(9);
            try
            {

                string connectionString = @"Data Source = 10.10.20.239,1433\sqlexpress; Initial Catalog = GEI_Expense_Client; User id=WebAccess;Password=12genEXT96";
                SqlConnection connection = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand("Select * from tblExpenseDetail WHERE Name = '" + user + "' and WeekEndingDate = '" + weekEndDate + "'", connection);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                connection.Open();

                adapter.Fill(ds, ds.Tables[2].TableName);

                connection.Close();



                // ProcessingMode will be Either Remote or Local  
                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.SizeToReportContent = true;
                rptViewer.ZoomMode = ZoomMode.FullPage;
               // rptViewer.Width = Unit.Percentage(99);
               // rptViewer.Height = Unit.Pixel(1000);                
                rptViewer.AsyncRendering = false;

                rptViewer.LocalReport.ReportPath = @"C:\inetpub\wwwroot\Intranet\Reports\ExpenseReportByDate.rdlc";
                ReportDataSource src = new ReportDataSource("ExpenseReportByDate", ds.Tables[2]);
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(src);


                ViewBag.ReportViewer = rptViewer;
                return View();
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        [HttpGet]
        public ActionResult ReturnReport(string time)
        {
            DateTime p = DateTime.Parse(time);
            DateTime e = WeekEnd(p);
            ReportViewer reportViewer = new ReportViewer();
            GEI_Expense_ClientDataSet ds = new GEI_Expense_ClientDataSet();
            GEIConnection db = new GEIConnection();
            string user = User.Identity.Name.Substring(9);
            try
            {

                string connectionString = @"Data Source = 10.10.20.239,1433\sqlexpress; Initial Catalog = GEI_Expense_Client;User id=WebAccess;Password=12genEXT96";
                SqlConnection connection = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand("Select * from tblExpenseDetail WHERE Name = '" + user + "' and WeekEndingDate = '" + e + "'", connection);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                connection.Open();

                adapter.Fill(ds, ds.Tables[2].TableName);

                connection.Close();



                // ProcessingMode will be Either Remote or Local  
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.ZoomMode = ZoomMode.FullPage;

                reportViewer.AsyncRendering = false;

                reportViewer.LocalReport.ReportPath = @"C:\inetpub\wwwroot\Intranet\Reports\PreviousReports.rdlc";
                ReportDataSource src = new ReportDataSource("PreviousReport", ds.Tables[2]);
                reportViewer.LocalReport.DataSources.Clear();
                reportViewer.LocalReport.DataSources.Add(src);


                ViewBag.ReportViewer = reportViewer;
                return View("ReturnReport");

            }
            catch (Exception x)
            {
                throw x;
            }
        }
                
        public ActionResult PreviousReports()
        {
            
            return View();
        }
        public ActionResult Modal()
        {
            return PartialView("ReportPartial");
        }
    }
}