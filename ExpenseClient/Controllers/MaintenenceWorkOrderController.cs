﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExpenseClient.ViewModels;
using ExpenseClient.Models;
using System.Net.Mail;
using System.Net;
using Newtonsoft.Json;
using System.Text;
using System.IO;
using System.Web.Script.Serialization;

namespace ExpenseClient.Controllers
{
    public class MaintenenceWorkOrderController : Controller
    {
        private static string authKey = "AIzaSyA-Fpzmjmn6YLB2yLNFpfa1s5JlKvq7HtY";
        private static string serverapiKey = "AIzaSyDTY4SIOnSbbxGgsh9Fgs58A7sKZy9VO7c";
        private static string senderID = "353125353566";

        private MaintWOEntities db = new MaintWOEntities();

        public List<MNTUrgencyDropDown> GetDetail()
        {
            List<MNTUrgencyDropDown> results = new List<MNTUrgencyDropDown>()
            {
                new MNTUrgencyDropDown(){ Urgency = "Routine" },
                new MNTUrgencyDropDown(){ Urgency = "ASAP" },
                new MNTUrgencyDropDown(){ Urgency = "EMERGENCY" },
            };          
            
            return results;            
        }

        public List<MachineDropDown> GetMachines()
        {
            List<MachineDropDown> results = new List<MachineDropDown>();          

            var obj = db.tblMachines.Select(m => m).ToList();
            if (obj != null && obj.Count() > 0)
            {
                foreach (var data in obj)
                {
                    MachineDropDown model = new MachineDropDown();
                    model.Machine = data.Mach_Desc;
                    model.MachineID = data.Mach_Num;
                    results.Add(model);
                    
                }
            }

            return results;
        }

        public List<DeptDropDown> GetDept()
        {
            List<DeptDropDown> results = new List<DeptDropDown>();

            var obj = db.tblDepartments.Select(m => m).ToList();
            if (obj != null && obj.Count() > 0)
            {
                foreach (var data in obj)
                {
                    DeptDropDown model = new DeptDropDown();
                    model.Dept = data.Dept_Desc;
                    model.DeptID = data.Dept_Num;
                    results.Add(model);

                }
            }
            return results;

        }

        // GET: Maintanence
        public ActionResult Index()
        {
            string user = User.Identity.Name.Substring(9);
            MaintWorkOrderViewModel model = new MaintWorkOrderViewModel();
            model.MachineDropDown = GetMachines();
            model.UrgencyDropDown = GetDetail();
            model.DeptDropDown = GetDept();
            model.Status = "Open";
            model.Date = DateTime.Now;
            model.Username = user;

            return View(model);
            
        }

        [HttpPost]
        public ActionResult Index(MaintWorkOrderViewModel model)
        {
            //post to db
            

            tblWO_HDR _db = new tblWO_HDR();
            _db.Date_Time_Submitted = model.Date;
            _db.WO_Dept_Num = model.DepartmentNum;
            _db.WO_Mach_Num = model.machineID.ToString();
            
            switch (model.Importance)           //emergency 0 asap 1 routine 2
            {
                case "Routine":
                    model.numericImportance = 2;
                    break;
                case "ASAP":
                    model.numericImportance = 1;
                    break;
                case "EMERGENCY":
                    model.numericImportance = 0;
                    break;
                
                                       
            }
            var obj = db.tblAssignees.Where(m => m.Logged_On == true).Select(m => m.FCM_Reg_ID);
            //List<string> loggedOnUsers = new List<string>();
            if (obj != null && obj.Count() > 0)
            {
                foreach (var data in obj)
                {
                    SendNotification("New Emergency Maintenence Request", "A new Emergency Maintenence Request has been added to the database. Please refer to your device for further information.", data);
                   // loggedOnUsers.Add(data);
                }
            }
            
            
            _db.WO_Urgency = model.numericImportance;
            _db.WO_Problem = model.Description;
            _db.WO_Submitted_By = model.Username;

            db.tblWO_HDR.Add(_db);
            db.SaveChanges();

            if (model.numericImportance == 0)
            {
                CreateCopyMessage("smtp.office365.com", model);
            }

            


            return RedirectToAction("Index");
        }

        public static void CreateCopyMessage(string server, MaintWorkOrderViewModel model)
        {
           
            MailAddress from = new MailAddress("do_not_reply@genext.com", "Alan Bradley");
            MailAddress to = new MailAddress("_GEIMaintenanceDept@genext.com"); //update to maint dept dist list
            MailMessage message = new MailMessage(from, to);
            
            message.IsBodyHtml = true;
            message.Subject = "A Document is waiting for you... ";
            message.Body = @"<strong>A new Maintenence Work Order Document has been assigned for your approval.</strong><br /><br /> " +
                "<strong>This document was originated by:</strong> " + model.Username + "<br />" +
                "<strong>Work Order Problem:</strong> " + model.Description + "<br />" +
                "<strong>Date submitted:</strong> " + DateTime.Now.ToShortDateString() + "<br />" +
                "<strong>Machine Name:</strong> " + model.machineID + "<br />" +
                "<strong>Urgency:</strong> " + model.Importance + "<br />" +
                "<strong>Status:</strong> " + model.Status + "<br />";
                
            // Add a carbon copy recipient.
            //MailAddress copy = new MailAddress("jdwhaley@genext.com", "John Whaley");
            //message.CC.Add(copy);
            SmtpClient client = new SmtpClient(server);
                        

            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public FCMPushNotification SendNotification(string _title, string _message, string userKey)
        {
            
            FCMPushNotification result = new FCMPushNotification();
            try
            {
                result.Successful = true;
                result.Error = null;
                // var value = message;
                var requestUri = "https://fcm.googleapis.com/fcm/send";

                WebRequest webRequest = WebRequest.Create(requestUri);
                webRequest.Method = "POST";
                webRequest.Headers.Add(string.Format("Authorization: key=" + authKey, serverapiKey)); //FCM SERVER API KEY
                webRequest.Headers.Add(string.Format("Sender: id={0}", senderID)); //FCM SENDERID
                webRequest.ContentType = "application/json";

                var data = new
                {
                    to = userKey, // Uncoment this if you want to test for single device
                                                                                                                                                                                        //to = "/topics/" + _topic, // this is for topic 
                    notification = new
                    {
                        title = _title,
                        body = _message,
                        //icon="myicon"
                    }
                };

                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                webRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = webRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse webResponse = webRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = webResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                result.Response = sResponseFromServer;
                            }
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                result.Successful = false;
                result.Response = null;
                result.Error = ex;
            }
            return result;
        }

    }
}