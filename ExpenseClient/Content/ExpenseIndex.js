﻿//GET data for jQuery Datatables 
$(document).ready(function () {

    GetEmployeeRecord();
})
//GET data for jQuery Datatables
var GetEmployeeRecord = function () {

    $.ajax({

        type: "Get",
        url: "/Expense/LoadData",
        success: function (response) {

            BindDataTable(response);

        }
    })

}

//Fill Datatable with JSON response
var BindDataTable = function (response) {

    $('#myTable').DataTable({
        "data": response,
        //searching: false,
        "columns": [
            { "data": "Mileage", "autoWidth": true },
            { "data": "Amount", "autoWidth": true },
            { "data": "Attendees", "autoWidth": true },
            { "data": "Place", "autoWidth": true },
            { "data": "PersonsEntertained", "autoWidth": true },
            { "data": "Purpose", "autoWidth": true },
            { "data": "Reimburse", "autoWidth": true },
            {
                "data": "Line",
                "render": function (Line, type, full, meta) {
                    //return '<a class="btn btn-info" onclick="EditExpense(' + Line + ')">Edit</a>';
                    return '<a class="btn" onclick="EditExpense(' + Line + ')"><span class="glyphicon glyphicon-pencil" /></a>';
                }

            },
            {
                "data": "Line",
                "render": function (Line, type, full, meta) {
                    //return '<a class="btn btn-danger" onclick="DeleteRow(' + Line + ')">Delete</a>';
                    return '<a class="btn" onclick="DeleteRow(' + Line + ')"><span class="glyphicon glyphicon-remove" /></a>';
                }
            }
        ]

    });
}

//Execute edit, displays Modal1
var EditExpense = function (line) {

    var url = "/Expense/EditExpense?Line=" + line;

    $("#myModalBodyDiv1").load(url, function () {

        $("#myModal1").modal({ show: true });


    })

}



//Executes Delete where model.Line == Line
function DeleteRow(Line) {
    if (confirm("Are you sure you want to delete this record..?")) {
        $.ajax({

            type: 'POST',
            url: "/Expense/delete?Line=" + Line,
            datatype: 'JSON',
            success: function (response) {
                if (response == "success") {

                    alert("Data deleted successfully..");
                    window.location.reload();
                    //$("#myModal").modal('hide');
                }
            },
            error: function (msg) {
                alert(msg.responseText);
            }

        });
    }
}


//f(x) to remove fade class on IE browsers
$(function () {
    var isIE = window.ActiveXObject || "ActiveXObject" in window;
    if (isIE) {
        $('.modal').removeClass('fade');
    }
});

//f(x) to hide DOM objects based on user selection
function ShowDiv() {
    $("#categoryid").on("change", function () {
        if ($("#categoryid option:selected").val() == 1) {
            $("#Amount").hide();
            $("#Mileage").show();
        }
        else {
            $("#Amount").show();
            $("#Mileage").hide();
        }

    })
};