﻿CREATE PROCEDURE [dbo].[AddExpenseDetails]
	@amount float,
	@purpose varchar (255),
	@mileage float,
	@category int,
	@expensedate datetime,
	@attendees int,
	@place varchar (100),
	@persons varchar (255)
AS
	begin
	Insert into tblExpenseDetail values (@amount,@purpose,@mileage,@category,@expensedate,@attendees,@place,@persons)
End
